$page.setTitle("Restricted Medical and Family History Details")

#parse("/screens/HcpRestrictedReportTop.vm")

#set($currentTier = $data.getSession().getAttribute("tier"))
#set($tiersMap = $data.getSession().getAttribute("userTiers"))
#set($project = $data.getSession().getAttribute("project"))
#set($projectAccess = $userTiersMap.get($project).get("accessLevel"))


<div class="report-info-block" id="restricted-data">
    ## Either the user does not have Restricted Access for this project
    #if($projectAccess <= 0)
        <br><p>This is restricted access data. You must obtain access before you will be able to view it.</p>
    ## They have it but don't have the tier dropdown properly set
    #elseif($projectAccess > 0 && $currentTier < 2)
        <br><p>This is sensitive data. In order to view it you must change your data view to sensitive by selecting the option from the dropdown in the Current Data View box.</p
    ## Or everything is good to view Sensitive Data
    #elseif($projectAccess > 0 && $currentTier > 1)

        <h2 class="expts_header">Intake Medical Information</h2>
        <table>
          <thead>
            <th width="60%">Condition</th>
            <th width="40%" style="text-align:center">Value</th>
          </thead>
          <tbody>
            <tr>
                <td>Blood Pressure</td>
                <td class="center">
                    #if ($!item.getIntegerProperty("hcp:restrictedMedFH/BP_Sys"))
                    $!item.getIntegerProperty("hcp:restrictedMedFH/BP_Sys")/$!item.getIntegerProperty("hcp:restrictedMedFH/BP_Dias")
                    #end
                </td>
            </tr>
            <tr>
                <td>Hematocrit - Sample 1</td>
                <td class="center">
                    #if ($!item.getIntegerProperty("hcp:restrictedMedFH/HematocritSample1"))
                    $!item.getIntegerProperty("hcp:restrictedMedFH/HematocritSample1")%
                    #end
                </td>
            </tr>
            <tr>
                <td>Hematocrit - Sample 2</td>
                <td class="center">
                    #if ($!item.getIntegerProperty("hcp:restrictedMedFH/HematocritSample2"))
                    $!item.getIntegerProperty("hcp:restrictedMedFH/HematocritSample2")%
                    #end
                </td>
            </tr>
            <tr>
                <td>Hematocrit - Source</td>
                <td class="center">
                    #set($value = $!item.getStringProperty("hcp:restrictedMedFH/HematocritSource"))
                    #if ($value == "F")
                        #set($displayedValue="F - Finger stick")
                    #elseif ($value == "B")
                        #set($displayedValue="B - Blood draw tube")
                    #else
                        #set($displayedValue="")
                    #end
                    $!displayedValue
                </td>
            </tr>
            <tr>
                <td>Blood Drawn</td>
                <td class="center">
                    $!item.getIntegerProperty("hcp:restrictedMedFH/BloodDrawn")
                </td>
            </tr>
            <tr>
                <td>Thyroid Stimulating Hormone</td>
                <td class="center">
                    #if ($!item.getFloatProperty("hcp:restrictedMedFH/TSH"))
                    $!item.getFloatProperty("hcp:restrictedMedFH/TSH") mclUnit/ml
                    #end
                </td>
            </tr>
            <tr>
                <td>Hemoglobin A1C (HbA1C)</td>
                <td class="center">
                    #if ($!item.getFloatProperty("hcp:restrictedMedFH/HBA1C"))
                    $!item.getFloatProperty("hcp:restrictedMedFH/HBA1C")%
                    #end
                </td>
            </tr>
          </tbody>
        </table>

        <br/>
        <br/><h2 class="expts_header">Intake Family History Data</h2>
        <table>
          <thead>
            <th width="60%">Item</th>
            <th width="20%" style="text-align:center">Mother</th>
            <th width="20%" style="text-align:center">Father</th>
          </thead>
          <tbody>
            <tr>
                <td>Schizophrenia or Psychosis</td>
                <td class="center">
                    $!item.getStringProperty("hcp:restrictedMedFH/FamHistMoth_Schiz")
                </td>
                <td class="center">
                    $!item.getStringProperty("hcp:restrictedMedFH/FamHistFath_Schiz")
                </td>
            </tr>
            <tr>
                <td>Depression</td>
                <td class="center">
                    $!item.getStringProperty("hcp:restrictedMedFH/FamHistMoth_Dep")
                </td>
                <td class="center">
                    $!item.getStringProperty("hcp:restrictedMedFH/FamHistFath_Dep")
                </td>
            </tr>
            <tr>
                <td>Bipolar Disorder</td>
                <td class="center">
                    $!item.getStringProperty("hcp:restrictedMedFH/FamHistMoth_Bip")
                </td>
                <td class="center">
                    $!item.getStringProperty("hcp:restrictedMedFH/FamHistFath_Bip")
                </td>
            </tr>
            <tr>
                <td>Anxiety that needed treatment</td>
                <td class="center">
                    $!item.getStringProperty("hcp:restrictedMedFH/FamHistMoth_Anx")
                </td>
                <td class="center">
                    $!item.getStringProperty("hcp:restrictedMedFH/FamHistFath_Anx")
                </td>
            </tr>
            <tr>
                <td>Drug or alcohol problems</td>
                <td class="center">
                    $!item.getStringProperty("hcp:restrictedMedFH/FamHistMoth_Drg")
                </td>
                <td class="center">
                    $!item.getStringProperty("hcp:restrictedMedFH/FamHistFath_Drg")
                </td>
            </tr>
            <tr>
                <td>Alzheimer's Disease or Dementia</td>
                <td class="center">
                    $!item.getStringProperty("hcp:restrictedMedFH/FamHistMoth_Alz")
                </td>
                <td class="center">
                    $!item.getStringProperty("hcp:restrictedMedFH/FamHistFath_Alz")
                </td>
            </tr>
            <tr>
                <td>Parkinson's Disease</td>
                <td class="center">
                    $!item.getStringProperty("hcp:restrictedMedFH/FamHistMoth_Prk")
                </td>
                <td class="center">
                    $!item.getStringProperty("hcp:restrictedMedFH/FamHistFath_Prk")
                </td>
            </tr>
            <tr>
                <td>Tourette's Syndrome</td>
                <td class="center">
                    $!item.getStringProperty("hcp:restrictedMedFH/FamHistMoth_Tour")
                </td>
                <td class="center">
                    $!item.getStringProperty("hcp:restrictedMedFH/FamHistFath_Tour")
                </td>
            </tr>
            <tr>
                <td><b>NONE OF THE ABOVE</b></td>
                <td class="center">
                    $!item.getStringProperty("hcp:restrictedMedFH/FamHistMoth_None")
                </td>
                <td class="center">
                    $!item.getStringProperty("hcp:restrictedMedFH/FamHistFath_None")
                </td>
            </tr>
          </tbody>
        </table>

            #if (!$!subject.GenderText.equalsIgnoreCase("Male"))
        <br/>
        <br/><h2 class="expts_header">Intake Menstrual Information</h2>
        <table>
          <thead>
            <th width="60%">Item</th>
            <th width="40%" style="text-align:center">Value</th>
          </thead>
          <tbody>
            <tr>
                <td>Endocrine problems - Hypothyroidism</td>
                <td class="center">
                    $!item.getStringProperty("hcp:restrictedMedFH/Menst_HypoT")
                </td>
            </tr>
            <tr>
                <td>Endocrine problems - Hypothyroidism - Age at onset</td>
                <td class="center">
                    $!item.getStringProperty("hcp:restrictedMedFH/Menst_HypoT_Ons")
                </td>
            </tr>
            <tr>
                <td>Endocrine problems - Hyperthyroidism</td>
                <td class="center">
                    $!item.getStringProperty("hcp:restrictedMedFH/Menst_HyperT")
                </td>
            </tr>
            <tr>
                <td>Endocrine problems - Hyperthyroidism - Age at onset</td>
                <td class="center">
                    $!item.getStringProperty("hcp:restrictedMedFH/Menst_HyperT_Ons")
                </td>
            </tr>
            <tr>
                <td>Endocrine problems - Other</td>
                <td class="center">
                    $!item.getStringProperty("hcp:restrictedMedFH/Menst_OtherE")
                </td>
            </tr>
            <tr>
                <td>Endocrine problems - Other - Age at onset</td>
                <td class="center">
                    $!item.getStringProperty("hcp:restrictedMedFH/Menst_Other_Ons")
                </td>
            </tr>
            <tr>
                <td>Regular menstrual cycles?</td>
                <td class="center">
                    $!item.getStringProperty("hcp:restrictedMedFH/Menst_Reg")
                </td>
            </tr>
            <tr>
                <td>Regular menstrual cycle - (if no, explain)</td>
                <td class="center">
                    $!item.getStringProperty("hcp:restrictedMedFH/Menst_RegExplain")
                </td>
            </tr>
            <tr>
                <td>Age menstrual cycles began</td>
                <td class="center">
                    $!item.getStringProperty("hcp:restrictedMedFH/Menst_AgeBegin")
                </td>
            </tr>
            <tr>
                <td>Length of menstrual cycle</td>
                <td class="center">
                    #set($value = $!item.getIntegerProperty("hcp:restrictedMedFH/Menst_Length"))
                    #if ($value == 1)
                        #set($displayedValue="1 - Less than 25 days")
                    #elseif ($value == 2)
                        #set($displayedValue="2 - Between 25-35 days")
                    #elseif ($value == 3)
                        #set($displayedValue="3 - More than every 35 days")
                    #else
                        #set($displayedValue="")
                    #end
                    $!displayedValue
                </td>
            </tr>
            <tr>
                <td>Days since last menstruation</td>
                <td class="center">
                    $!item.getStringProperty("hcp:restrictedMedFH/Menst_DaysSinceLast")
                </td>
            </tr>
            <tr>
                <td>Age cycles became irregular</td>
                <td class="center">
                    $!item.getStringProperty("hcp:restrictedMedFH/Menst_AgeIrreg")
                </td>
            </tr>
            <tr>
                <td>Age cycles stopped</td>
                <td class="center">
                    $!item.getStringProperty("hcp:restrictedMedFH/Menst_AgeStop")
                </td>
            </tr>
            <tr>
                <td>If amenorrheic, estimated total months of amenorrhea sence menarch</td>
                <td class="center">
                    $!item.getStringProperty("hcp:restrictedMedFH/Menst_Menarch")
                </td>
            </tr>
            <tr>
                <td>Using birth control pills, progesterone, or fertility drugs</td>
                <td class="center">
                    $!item.getStringProperty("hcp:restrictedMedFH/Menst_BCFert")
                </td>
            </tr>
            <tr>
                <td>If yes, drug</td>
                <td class="center">
                    #set($value = $!item.getIntegerProperty("hcp:restrictedMedFH/Menst_BCFCode"))
                    #if ($value == 1)
                        #set($displayedValue="1 - OC's for contraception")
                    #elseif ($value == 2)
                        #set($displayedValue="2 - OC's primarily for menstrual regulation")
                    #elseif ($value == 3)
                        #set($displayedValue="3 - Estradiol for menstrual regulation")
                    #elseif ($value == 4)
                        #set($displayedValue="4 - Progesterone for menstrual regulation")
                    #elseif ($value == 5)
                        #set($displayedValue="5 - Fertility therapy")
                    #elseif ($value == 6)
                        #set($displayedValue="6 - Other")
                    #elseif ($value == 7)
                        #set($displayedValue="7 - Unknown")
                    #else
                        #set($displayedValue="")
                    #end
                    $!displayedValue
                </td>
            </tr>
          </tbody>
        </table>
            #end
    #end
</div>
<!-- END hcp:restrictedMedFH -->

#parse("/screens/ReportProjectSpecificFields.vm")




