<!-- BEGIN XDATScreen_report_nt_scores.vm -->
$page.setTitle("Non-Toolbox Scoring Details")
#parse("/screens/HcpReportTop.vm")

<div class="report-info-block">
	
	<h2 class="expts_header">HCPPNP (Mars Letter Contrast Sensitivity) Data</h2>
	<table>
	  <thead>
		<th width="75%">Measure</th><th width="25%">Score</th>
	  </thead>
	  
	  <tbody>
		<tr>
			<td>MARS Log CS value at final correct letter</td>
			<td>$!item.getStringProperty("nt:scores/HCPPNP/mars_log_score")</td>
		</tr>
		<tr>
			<td>MARS number of errors prior to final correct letter</td>
			<td>$!item.getStringProperty("nt:scores/HCPPNP/mars_errs")</td>
		</tr>
		<tr>
			<td>MARS Log Contrast Sensitivity Final Score</td>
			<td>$!item.getStringProperty("nt:scores/HCPPNP/mars_final")</td>
		</tr>
	  </tbody>
	</table>
	
	<h2 class="expts_header">DDISC (Delay Discounting) Data</h2>
	<table>
	  <thead>
		<th width="75%">Measure</th><th width="25%">Score</th>
	  </thead>
	  
	  <tbody>
	  	<tr>
			<td>Subject Value for Delay of 1mo, Delay Amount $200</td>
			<td>$!item.getDoubleProperty("nt:scores/DDISC/SV_1mo_200")</td>
		</tr>
		<tr>
			<td>Subject Value for Delay of 6mo, Delay Amount $200</td>
			<td>$!item.getDoubleProperty("nt:scores/DDISC/SV_6mo_200")</td>
		</tr>
		<tr>
			<td>Subject Value for Delay of 1yr, Delay Amount $200</td>
			<td>$!item.getDoubleProperty("nt:scores/DDISC/SV_1yr_200")</td>
		</tr>
		<tr>
			<td>Subject Value for Delay of 3yr, Delay Amount $200</td>
			<td>$!item.getDoubleProperty("nt:scores/DDISC/SV_3yr_200")</td>
		</tr>
		<tr>
			<td>Subject Value for Delay of 5yr, Delay Amount $200</td>
			<td>$!item.getDoubleProperty("nt:scores/DDISC/SV_5yr_200")</td>
		</tr>
		<tr>
			<td>Subject Value for Delay of 10yr, Delay Amount $200</td>
			<td>$!item.getDoubleProperty("nt:scores/DDISC/SV_10yr_200")</td>
		</tr>
		<tr>
			<td>Subject Value for Delay of 1mo, Delay Amount $40000</td>
			<td>$!item.getDoubleProperty("nt:scores/DDISC/SV_1mo_40000")</td>
		</tr>
		<tr>
			<td>Subject Value for Delay of 6mo, Delay Amount $40000</td>
			<td>$!item.getDoubleProperty("nt:scores/DDISC/SV_6mo_40000")</td>
		</tr>
		<tr>
			<td>Subject Value for Delay of 1yr, Delay Amount $40000</td>
			<td>$!item.getDoubleProperty("nt:scores/DDISC/SV_1yr_40000")</td>
		</tr>
		<tr>
			<td>Subject Value for Delay of 3yr, Delay Amount $40000</td>
			<td>$!item.getDoubleProperty("nt:scores/DDISC/SV_3yr_40000")</td>
		</tr>
		<tr>
			<td>Subject Value for Delay of 5yr, Delay Amount $40000</td>
			<td>$!item.getDoubleProperty("nt:scores/DDISC/SV_5yr_40000")</td>
		</tr>
		<tr>
			<td>Subject Value for Delay of 10yr, Delay Amount $40000</td>
			<td>$!item.getDoubleProperty("nt:scores/DDISC/SV_10yr_40000")</td>
		</tr>
		<tr>
			<td>Area Under the Curve for Delay Amount $200</td>
			<td>$!item.getDoubleProperty("nt:scores/DDISC/AUC_200")</td>
		</tr>
		<tr>
			<td>Area Under the Curve for Delay Amount $40000</td>
			<td>$!item.getDoubleProperty("nt:scores/DDISC/AUC_40000")</td>
		</tr>
	  </tbody>
	</table>

	<h2 class="expts_header">NEO (NEO-FFI Five Factor Personality Inventory) Data</h2>
	<table>
	  <thead>
		<th width="75%">Measure</th><th width="25%">Score</th>
	  </thead>
	  
	  <tbody>
		<tr>
			<td>Combined Factor (Total) Score</td>
			<td>$!item.getStringProperty("nt:scores/NEO/NEO")</td>
		</tr>
		<tr>
			<td>Factor A (Agreeableness)</td>
			<td>$!item.getStringProperty("nt:scores/NEO/NEOFAC_A")</td>
		</tr>
		<tr>
			<td>Factor O (Openness to Experience)</td>
			<td>$!item.getStringProperty("nt:scores/NEO/NEOFAC_O")</td>
		</tr>
		<tr>
			<td>Factor C (Conscientiousness)</td>
			<td>$!item.getStringProperty("nt:scores/NEO/NEOFAC_C")</td>
		</tr>
		<tr>
			<td>Factor N (Neuroticism)</td>
			<td>$!item.getStringProperty("nt:scores/NEO/NEOFAC_N")</td>
		</tr>
		<tr>
			<td>Factor E (Extraversion)</td>
			<td>$!item.getStringProperty("nt:scores/NEO/NEOFAC_E")</td>
		</tr>
	  </tbody>
	</table>
	
	<h2 class="expts_header">SPCPTNL (Short Continuous Performance Test) Data</h2>
	<table>
	  <thead>
		<th width="75%">Measure</th><th width="25%">Score</th>
	  </thead>
	  
	  <tbody>
		<tr>
			<td>Short CPT True Positives = Sum of CPN_TP and CPL_TP</td>
			<td>$!item.getStringProperty("nt:scores/SPCPTNL/SCPT_TP")</td>
		</tr>
		<tr>
			<td>Short CPT True Negatives = Sum of CPN_TN and CPL_TN</td>
			<td>$!item.getStringProperty("nt:scores/SPCPTNL/SCPT_TN")</td>
		</tr>
		<tr>
			<td>Short CPT False Positives = Sum of CPN_FP and CPL_FP</td>
			<td>$!item.getStringProperty("nt:scores/SPCPTNL/SCPT_FP")</td>
		</tr>
		<tr>
			<td>Short CPT False Negatives = Sum of CPN_FN and CPL_FN</td>
			<td>$!item.getStringProperty("nt:scores/SPCPTNL/SCPT_FN")</td>
		</tr>
		<tr>
			<td>Median Response Time for Short CPT True Positive Responses</td>
			<td>$!item.getDoubleProperty("nt:scores/SPCPTNL/SCPT_TPRT")</td>
		</tr>
		<tr>
			<td>Sensitivity = SCPT_TP/(SCPT_TP + SCPT_FN)</td>
			<td>$!item.getDoubleProperty("nt:scores/SPCPTNL/SCPT_SEN")</td>
		</tr>
		<tr>
			<td>Specificity = SCPT_TN/(SCPT_TN + SCPT_FP)</td>
			<td>$!item.getDoubleProperty("nt:scores/SPCPTNL/SCPT_SPEC")</td>
		</tr>
		<tr>
			<td>NumLet CPT (Longest Run of Non-Responses)</td>
			<td>$!item.getStringProperty("nt:scores/SPCPTNL/SCPT_LRNR")</td>
		</tr>
	  </tbody>
	</table>

	<h2 class="expts_header">CPW (Computerized Penn Word Memory) Data</h2>
	<table>
	  <thead>
		<th width="75%">Measure</th><th width="25%">Score</th>
	  </thead>
	  
	  <tbody>
		<tr>
			<td>CPW Total Correct Reponses</td>
			<td>$!item.getStringProperty("nt:scores/CPW/IWRD_TOT")</td>
		</tr>
		<tr>
			<td>Median Response Time for CPW Total Correct Reponses (ms)</td>
			<td>$!item.getStringProperty("nt:scores/CPW/IWRD_RTC")</td>
		</tr>
	  </tbody>
	</table>

	<h2 class="expts_header">PMAT24A (Progressive Matricies) Data</h2>
	<table>
	  <thead>
		<th width="75%">Measure</th><th width="25%">Score</th>
	  </thead>
	  
	  <tbody>
		<tr>
			<td>PMAT24 Correct Reponses for Form A</td>
			<td>$!item.getStringProperty("nt:scores/PMAT24A/PMAT24_A_CR")</td>
		</tr>
		<tr>
			<td>PMAT24 Total Skipped Items (items not presented because maximum errors allowed reached) for Form A</td>
			<td>$!item.getStringProperty("nt:scores/PMAT24A/PMAT24_A_SI")</td>
		</tr>
		<tr>
			<td>Median Response Time for Correct PMAT24 Reponses (ms)</td>
			<td>$!item.getStringProperty("nt:scores/PMAT24A/PMAT24_A_RTCR")</td>
		</tr>
	  </tbody>
	</table>

	<h2 class="expts_header">VSPLOT24 (Visiospatial) Data</h2>
	<table>
	  <thead>
		<th width="75%">Measure</th><th width="25%">Score</th>
	  </thead>
	  
	  <tbody>
		<tr>
			<td>Total Correct</td>
			<td>$!item.getStringProperty("nt:scores/VSPLOT24/VSPLOT_TC")</td>
		</tr>
		<tr>
			<td>Median of Redsponse Time Divided by Expected Number of Clicks for Correct Trials</td>
			<td>$!item.getDoubleProperty("nt:scores/VSPLOT24/VSPLOT_CRTE")</td>
		</tr>
		<tr>
			<td>Total Positions Off for All Trials</td>
			<td>$!item.getStringProperty("nt:scores/VSPLOT24/VSPLOT_OFF")</td>
		</tr>
	  </tbody>
	</table>
		
	<h2 class="expts_header">ER40 (Expression Recognition) Data</h2>
	<table>
	  <thead>
		<th width="75%">Measure</th><th width="25%">Score</th>
	  </thead>
	  
	  <tbody>
		<tr>
			<td>ER40 Correct Responses</td>
			<td>$!item.getStringProperty("nt:scores/ER40/ER40_CR")</td>
		</tr>
		<tr>
			<td>ER40 Correct Responses Median Response Time (ms)</td>
			<td>$!item.getStringProperty("nt:scores/ER40/ER40_CRT")</td>
		</tr>
		<tr>
			<td>ER40 Correct Anger Identifications</td>
			<td>$!item.getStringProperty("nt:scores/ER40/ER40ANG")</td>
		</tr>
		<tr>
			<td>ER40 Correct Fear Identifications</td>
			<td>$!item.getStringProperty("nt:scores/ER40/ER40FEAR")</td>
		</tr>
		<tr>
			<td>ER40 Correct Happy Identifications</td>
			<td>$!item.getStringProperty("nt:scores/ER40/ER40HAP")</td>
		</tr>
		<tr>
			<td>ER40 Correct Neutral Identifications</td>
			<td>$!item.getStringProperty("nt:scores/ER40/ER40NOE")</td>
		</tr>
		<tr>
			<td>ER40 Correct Sad Identifications</td>
			<td>$!item.getStringProperty("nt:scores/ER40/ER40SAD")</td>
		</tr>
	  </tbody>
	</table>
</div>

#parse("/screens/ReportProjectSpecificFields.vm")

<!-- END XDATScreen_report_nt_scores.vm -->
