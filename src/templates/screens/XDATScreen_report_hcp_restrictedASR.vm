<!-- begin XDATScreen_report_hcp_restrictedASR.vm -->
$page.setTitle("Restricted ASR Scoring Details")

#parse("/screens/HcpRestrictedReportTop.vm")

#set($currentTier = $data.getSession().getAttribute("tier"))
#set($tiersMap = $data.getSession().getAttribute("userTiers"))
#set($project = $data.getSession().getAttribute("project"))
#set($projectAccess = $userTiersMap.get($project).get("accessLevel"))

<div class="report-info-block" id="restricted-data">
	## Either the user does not have Restricted Access for this project
	#if($projectAccess <= 0)
        <br><p>This is restricted access data. You must obtain access before you will be able to view it.</p>
	## They have it but don't have the tier dropdown properly set
	#elseif($projectAccess > 0 && $currentTier < 2)
        <br><p>This is sensitive data. In order to view it you must change your data view to sensitive by selecting the option from the dropdown in the Current Data View box.</p
	## Or everything is good to view Sensitive Data
	#elseif($projectAccess > 0 && $currentTier > 1)

	<h2 class="expts_header">Restricted Adult Self Report (ASR) Scoring Details</h2>

	<br><p><strong>ASR Total Score</strong></p>
	<table>
	  <thead>
		<th width="50%">Measure</th>
		<th width="25%">Raw Score</th>
		<th width="25%">T-Score</th>
		<!--
			<th class="underscore">Notation</th>
		-->
	  </thead>
	  
	  <tbody>
		<tr>
			<td>Computed Sum (Total)</td>
			<td>$!item.getIntegerProperty("hcp:restrictedASR/ASRSyndromeScores/ASR_cmp_total_raw")</td>
			<td>$!item.getIntegerProperty("hcp:restrictedASR/ASRSyndromeScores/ASR_cmp_total_t")</td>
		<!--
			<td>$!item.getStringProperty("hcp:restrictedASR/ASRSyndromeScores/ASR_cmp_total_note")</td>
		-->
		</tr>
	  </tbody>
	</table>
	
	<p><strong>Internalizing/Externalizing/Other Scores</strong></p>
	<table>
	  <thead>
		<th width="50%">Measure</th>
		<th width="25%">Raw Score</th>
		<th width="25%">T-Score</th>
		<!--
			<th class="underscore">Notation</th>
		-->
	  </thead>
	  
	  <tbody>
		<tr>
			<td>Internalizing</td>
			<td>$!item.getIntegerProperty("hcp:restrictedASR/ASRSyndromeScores/ASR_cmp_internalizing_raw")</td>
			<td>$!item.getIntegerProperty("hcp:restrictedASR/ASRSyndromeScores/ASR_cmp_internalizing_t")</td>
		<!--
			<td>$!item.getStringProperty("hcp:restrictedASR/ASRSyndromeScores/ASR_cmp_internalizing_note")</td>
		-->
		</tr>
		<tr>
			<td>Externalizing</td>
			<td>$!item.getIntegerProperty("hcp:restrictedASR/ASRSyndromeScores/ASR_cmp_externalizing_raw")</td>
			<td>$!item.getIntegerProperty("hcp:restrictedASR/ASRSyndromeScores/ASR_cmp_externalizing_t")</td>
		<!--
			<td>$!item.getStringProperty("hcp:restrictedASR/ASRSyndromeScores/ASR_cmp_externalizing_note")</td>
		-->
		</tr>
		<tr>
			<td>Thought/Attention/Other</td>
			<td>$!item.getIntegerProperty("hcp:restrictedASR/ASRSyndromeScores/ASR_cmp_other_raw")</td>
			<td>$!item.getIntegerProperty("hcp:restrictedASR/ASRSyndromeScores/ASR_cmp_other_t")</td>
		<!--
			<td>$!item.getStringProperty("hcp:restrictedASR/ASRSyndromeScores/ASR_cmp_other_note")</td>
		-->
		</tr>
	  </tbody>
	</table>
	<p><strong>Problem Category Scores</strong></p>
	<table>
	  <thead>
		<th width="50%">Measure</th>
		<th width="25%">Raw Score</th>
		<th width="25%">T-Score</th>
		<!--
			<th class="underscore">Notation</th>
		-->
	  </thead>
	  
	  <tbody>
		<tr>
			<td>I. Anxious/Depressed</td>
			<td>$!item.getIntegerProperty("hcp:restrictedASR/ASRSyndromeScores/ASR_anxdp_raw")</td>
			<td>$!item.getIntegerProperty("hcp:restrictedASR/ASRSyndromeScores/ASR_anxdp_t")</td>
		<!--
			<td>$!item.getStringProperty("hcp:restrictedASR/ASRSyndromeScores/ASR_anxdp_note")</td>
		-->
		</tr>
		<tr>
			<td>II. Withdrawn-Depressed</td>
			<td>$!item.getIntegerProperty("hcp:restrictedASR/ASRSyndromeScores/ASR_wthdp_raw")</td>
			<td>$!item.getIntegerProperty("hcp:restrictedASR/ASRSyndromeScores/ASR_wthdp_t")</td>
		<!--
			<td>$!item.getStringProperty("hcp:restrictedASR/ASRSyndromeScores/ASR_wthdp_note")</td>
		-->
		</tr>
		<tr>
			<td>III. Somatic Complaints</td>
			<td>$!item.getIntegerProperty("hcp:restrictedASR/ASRSyndromeScores/ASR_som_raw")</td>
			<td>$!item.getIntegerProperty("hcp:restrictedASR/ASRSyndromeScores/ASR_som_t")</td>
		<!--
			<td>$!item.getStringProperty("hcp:restrictedASR/ASRSyndromeScores/ASR_som_note")</td>
		-->
		</tr>
		<tr>
			<td>IV. Thought Problems</td>
			<td>$!item.getIntegerProperty("hcp:restrictedASR/ASRSyndromeScores/ASR_tho_raw")</td>
			<td>$!item.getIntegerProperty("hcp:restrictedASR/ASRSyndromeScores/ASR_tho_t")</td>
		<!--
			<td>$!item.getStringProperty("hcp:restrictedASR/ASRSyndromeScores/ASR_tho_note")</td>
		-->
		</tr>
		<tr>
			<td>V. Attention Problems</td>
			<td>$!item.getIntegerProperty("hcp:restrictedASR/ASRSyndromeScores/ASR_att_raw")</td>
			<td>$!item.getIntegerProperty("hcp:restrictedASR/ASRSyndromeScores/ASR_att_t")</td>
		<!--
			<td>$!item.getStringProperty("hcp:restrictedASR/ASRSyndromeScores/ASR_att_note")</td>
		-->
		</tr>
		<tr>
			<td>VI. Aggressive Behavior</td>
			<td>$!item.getIntegerProperty("hcp:restrictedASR/ASRSyndromeScores/ASR_agg_raw")</td>
			<td>$!item.getIntegerProperty("hcp:restrictedASR/ASRSyndromeScores/ASR_agg_t")</td>
		<!--
			<td>$!item.getStringProperty("hcp:restrictedASR/ASRSyndromeScores/ASR_agg_note")</td>
		-->
		</tr>
		<tr>
			<td>VII. Rule-Breaking Behaviors</td>
			<td>$!item.getIntegerProperty("hcp:restrictedASR/ASRSyndromeScores/ASR_rule_raw")</td>
			<td>$!item.getIntegerProperty("hcp:restrictedASR/ASRSyndromeScores/ASR_rule_t")</td>
		<!--
			<td>$!item.getStringProperty("hcp:restrictedASR/ASRSyndromeScores/ASR_rule_note")</td>
		-->
		</tr>
		<tr>
			<td>VIII. Intrusive Thoughts</td>
			<td>$!item.getIntegerProperty("hcp:restrictedASR/ASRSyndromeScores/ASR_int_raw")</td>
			<td>$!item.getIntegerProperty("hcp:restrictedASR/ASRSyndromeScores/ASR_int_t")</td>
		<!--
			<td>$!item.getStringProperty("hcp:restrictedASR/ASRSyndromeScores/ASR_int_note")</td>
		-->
		</tr>							
		<tr>
			<td>Other Problems</td>
			<td>$!item.getIntegerProperty("hcp:restrictedASR/ASRSyndromeScores/ASR_other_raw")</td>
			<td></td>
			<td></td>
		</tr>							
		<tr>
			<td>Critical Items Sum</td>
			<td>$!item.getIntegerProperty("hcp:restrictedASR/ASRSyndromeScores/ASR_critical_raw")</td>
			<td></td>
			<td></td>
		</tr>			
		</tbody>				
	</table>
    
	<h2 class="expts_header">ASR DSM-Oriented Scale Computed Scores</h2>
    <br><p><strong>Problem Category Scores</strong></p>
	<table>
	  <thead>
		<th width="50%">Measure</th>
		<th width="25%">Raw Score</th>
		<th width="25%">T-Score</th>
		<!--
			<th class="underscore">Notation</th>
		-->
	  </thead>
	  
	  <tbody>
		<tr>
			<td>1. Depressive Problems</td>
			<td>$!item.getIntegerProperty("hcp:restrictedASR/ASRDsmScores/DSM_dep_raw")</td>
			<td>$!item.getIntegerProperty("hcp:restrictedASR/ASRDsmScores/DSM_dep_t")</td>
		<!--
			<td>$!item.getStringProperty("hcp:restrictedASR/ASRDsmScores/DSM_dep_note")</td>
		-->
		</tr>
		<tr>
			<td>2. Anxiety Problems</td>
			<td>$!item.getIntegerProperty("hcp:restrictedASR/ASRDsmScores/DSM_anx_raw")</td>
			<td>$!item.getIntegerProperty("hcp:restrictedASR/ASRDsmScores/DSM_anx_t")</td>
		<!--
			<td>$!item.getStringProperty("hcp:restrictedASR/ASRDsmScores/DSM_anx_note")</td>
		-->
		</tr>
		<tr>
			<td>3. Somatic Problems</td>
			<td>$!item.getIntegerProperty("hcp:restrictedASR/ASRDsmScores/DSM_som_raw")</td>
			<td>$!item.getIntegerProperty("hcp:restrictedASR/ASRDsmScores/DSM_som_t")</td>
		<!--
			<td>$!item.getStringProperty("hcp:restrictedASR/ASRDsmScores/DSM_som_note")</td>
		-->
		</tr>
		<tr>
			<td>4. Avoidant Personality Problems</td>
			<td>$!item.getIntegerProperty("hcp:restrictedASR/ASRDsmScores/DSM_avoid_raw")</td>
			<td>$!item.getIntegerProperty("hcp:restrictedASR/ASRDsmScores/DSM_avoid_t")</td>
		<!--
			<td>$!item.getStringProperty("hcp:restrictedASR/ASRDsmScores/DSM_avoid_note")</td>
		-->
		</tr>
		<tr>
			<td>5. AD/H Problems</td>
			<td>$!item.getIntegerProperty("hcp:restrictedASR/ASRDsmScores/DSM_adh_raw")</td>
			<td>$!item.getIntegerProperty("hcp:restrictedASR/ASRDsmScores/DSM_adh_t")</td>
		<!--
			<td>$!item.getStringProperty("hcp:restrictedASR/ASRDsmScores/DSM_adh_note")</td>
		-->
		</tr>
		<tr>
			<td style="padding-left:24px">5a. Inattention</td>
			<td>$!item.getIntegerProperty("hcp:restrictedASR/ASRDsmScores/DSM_inatt_raw")</td>
			<td></td>
		<!--
			<td>$!item.getStringProperty("hcp:restrictedASR/ASRDsmScores/DSM_inatt_note")</td>
		-->
		</tr>
		<tr>
			<td style="padding-left:24px">5b. Hyperactivity-Impulsivity</td>
			<td>$!item.getIntegerProperty("hcp:restrictedASR/ASRDsmScores/DSM_hyp_raw")</td>
			<td></td>
		<!--
			<td>$!item.getStringProperty("hcp:restrictedASR/ASRDsmScores/DSM_hyp_note")</td>
		-->
		</tr>
		<tr>
			<td>6. Antisocial Personality Problems</td>
			<td>$!item.getIntegerProperty("hcp:restrictedASR/ASRDsmScores/DSM_asoc_raw")</td>
			<td>$!item.getIntegerProperty("hcp:restrictedASR/ASRDsmScores/DSM_asoc_t")</td>
		<!--
			<td>$!item.getStringProperty("hcp:restrictedASR/ASRDsmScores/DSM_asoc_note")</td>
		-->
		</tr>
		</tbody>
	</table>
	#end
</div>
<!-- END hcp:restrictedASR -->

#parse("/screens/ReportProjectSpecificFields.vm")




