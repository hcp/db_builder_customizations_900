/**
 * Copyright (c) 2013 Washington University School of Medicine
 */
package org.nrg.xnat.download;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.web.servlet.view.AbstractView;

/**
 * Ugly ugly hack. View for a JSON.org JSONObject.
 * @author Kevin A. Archie <karchie@wustl.edu>
 *
 */
class JSONOrgView extends AbstractView {
    /* (non-Javadoc)
     * @see org.springframework.web.servlet.view.AbstractView#renderMergedOutputModel(java.util.Map, javax.servlet.http.HttpServletRequest, javax.servlet.http.HttpServletResponse)
     */
    @Override
    protected void renderMergedOutputModel(final Map<String, Object> model,
            final HttpServletRequest request,
            final HttpServletResponse response)
                    throws IOException,JSONException {
        final JSONObject json = (JSONObject)model.get("json");
        final PrintWriter w = response.getWriter();
        try {
            json.write(w);
        } finally {
            w.close();
        }
    }
}
