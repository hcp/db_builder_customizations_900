package org.nrg.xnat.download;

import org.nrg.xdat.XDAT;

import javax.inject.Inject;
import javax.inject.Named;
import java.io.File;
import java.io.Serializable;
import java.nio.file.Path;
import java.nio.file.Paths;

/**
 * Created with IntelliJ IDEA.
 * User: mmckay01
 * Date: 11/6/13
 * Time: 4:26 PM
 * To change this template use File | Settings | File Templates.
 */
public class RefreshPackagesRequest implements Serializable {

    private static final long serialVersionUID = 1L;

    private String subDir = null;

    private File packageRoot;

    private String packageSubdir;

    public RefreshPackagesRequest(String project){
        packageRoot = XDAT.getContextService().getBean(HCPPackageBuilderProvider.class).getPackageRootForProject(project);
        packageSubdir = XDAT.getContextService().getBean(HCPPackageBuilderProvider.class).getPackageSubdirForProject(project);
    }

    public String getSubDir(){
        return subDir;
    }

    public void setSubDir(String sub){
        subDir = sub;
    }

    public Path getPackageDir(){
        if(subDir!=null){
            return Paths.get(packageRoot.toPath().toString(), packageSubdir + "/" + subDir);
        }
        else{
            return Paths.get(packageRoot.toPath().toString(), packageSubdir);
        }
    }

}
