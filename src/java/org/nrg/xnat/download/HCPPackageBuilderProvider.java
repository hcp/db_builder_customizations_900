/**
 * Copyright (c) 2013 Washington University School of Medicine
 */
package org.nrg.xnat.download;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.URI;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.lang.StringUtils;
import org.nrg.xdat.XDAT;
import org.nrg.xnat.services.PackageFileService;
import org.nrg.xnat.entities.PackageFile;
import org.nrg.xnat.entities.PackageInfo;

import javax.inject.Inject;
import javax.inject.Named;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.nrg.xdat.security.XDATUser;
import org.springframework.security.access.AccessDeniedException;

import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableMap;

/**
 * Hard-coded package layout and metadata originally for HCP Q1 release. Yuck.
 *
 * @author Kevin A. Archie <karchie@wustl.edu>
 */
public class HCPPackageBuilderProvider implements PackageBuilderProvider {
    private static final String SECURING_ELEMENT = "xnat:mrSessionData/project";
    private static final String MD5_SUFFIX = ".md5";

    private final Logger logger = LoggerFactory.getLogger(HCPPackageBuilderProvider.class);
    // NOTE:  This is keeping a separate cache from the PackageFileService class because attempts to replace
    // the PackageFileService cache with this one and to use a single cache for both ran significantly slower.
    // NOTE:  Not using concurrenthashmap.  This cache needs to store null values.
    private final Map<String,Map<String,PackageFile>> pfCache = Collections.synchronizedMap(new HashMap<String,Map<String,PackageFile>>());

    @Inject
    @Named("packageRoots")
    private PackageRoot packageRoots;

    @Inject
    @Named("packageSubdirs")
    private PackageSubdir packageSubdirs;

    private String _suffix;

    // Use a linked hashmap to ensure order of packages.
    private LinkedHashMap<String, LinkedHashMap<String, PackageDescriptor>> _packages = new LinkedHashMap<String, LinkedHashMap<String, PackageDescriptor>>();

    public File getPackageRootForProject(String project) {
        return packageRoots.getRoot(project);
    }

    public String getPackageSubdirForProject(String project) {
        return packageSubdirs.getSubDir(project);
    }

    public String getSuffix() {
        return _suffix;
    }

    public void setSuffix(String suffix) {
        _suffix = suffix;
    }

    public void setDescriptors(final Map<String, List<PackageDescriptor>> descriptors) {
        _packages.clear();
        for (Map.Entry<String, List<PackageDescriptor>> projDescriptors : descriptors.entrySet()){
            String projName = projDescriptors.getKey();
            LinkedHashMap<String, PackageDescriptor> descriptorsForProject = new LinkedHashMap<String, PackageDescriptor>();
            for (PackageDescriptor descriptor : projDescriptors.getValue()) {
                descriptorsForProject.put(descriptor.getName(), descriptor);
            }
            _packages.put(projName, descriptorsForProject);
        }
    }
    
    /*
     * (non-Javadoc)
     * @see org.nrg.xnat.download.PackageBuilderProvider#getPackageNames()
     */
    public final Set<String> getPackageNames(final String project) {
        return _packages.get(project).keySet();
    }

    public final String getPackageLabel(final String project, final String packageName) {
        return _packages.get(project).get(packageName).getLabel();
    }

    /*
     * (non-Javadoc)
     * @see org.nrg.xnat.download.PackageBuilderProvider#getPackageDescription(java.lang.String)
     */
    public final String getPackageDescription(final String project, final String packageName) {
        return _packages.get(project).get(packageName).getDescription();
    }

    /*
     * (non-Javadoc)
     * @see org.nrg.xnat.download.PackageBuilderProvider#getPackageKeywords(java.lang.String)
     */
    public final Set<String> getPackageKeywords(final String project, final String packageName) {
        return _packages.get(project).get(packageName).getKeywords();
    }

    /*
     * (non-Javadoc)
     * @see com.google.common.base.Function#apply(java.lang.Object)
     */
    public PackageBuilder apply(final String project, final String packageName) {
        return new HCPZipFilePackageBuilder(packageRoots.getRoot(project).toPath(), packageName, getSuffix());
    }
    
    public void refreshPackageFileCache() {
    	synchronized(pfCache) {
    		for (String id : pfCache.keySet()) {
	          	final Map<String,PackageFile> idMap = Collections.synchronizedMap(pfCache.get(id));
    			synchronized(idMap) {
    				for (String pkg : idMap.keySet()) {
	    				idMap.put(pkg,XDAT.getContextService().getBean(PackageFileService.class).getPackageFileByPath(pkg));
	    			}
	    		}
	    	}
    	}
    }
    
    public void clearCache() {
    	synchronized(pfCache) {
    		pfCache.clear();
    	}
    }

	public void removeNullCacheValues() {
    	synchronized(pfCache) {
    		Iterator<String> siter = pfCache.keySet().iterator();
    		while (siter.hasNext()) {
    			String subj = siter.next();
    			if (pfCache.get(subj)==null) {
    				siter.remove();
    			} else {
					final Map<String,PackageFile> pmap = pfCache.get(subj);
					Iterator<String> piter = pmap.keySet().iterator();
					while (piter.hasNext()) {
						if (pmap.get(piter.next())==null) {
							piter.remove();
						}
					}
    			}
    		}
		}
	}
    
    public Map<String,PackageInfo> getPackageInfo(final Collection<String> projectPaths,final Collection<String> subjects) {
    	return XDAT.getContextService().getBean(PackageFileService.class).getPackageInfo(projectPaths,subjects);
    }
    
    public Long getCountByPackageIds(final Collection<String> projectPaths,final Collection<String> subjects,final Collection<String> packageIds) {
    	return XDAT.getContextService().getBean(PackageFileService.class).getCountByPackageIds(projectPaths,subjects,packageIds);
    }
    
    public class HCPZipFilePackageBuilder extends ZipFilePackageBuilder {
        private final String packageName;
        private final String suffix;
        
        
        protected class HCPZipFilePackage extends ZipFilePackage {
            private final String strroot;
            private final String nameFormat;
            private final String folder;
            private final String project;

            public HCPZipFilePackage(final Path root,
                                     final String id,
                                     final String packageName,
                                     final String suffix,
                                     final Map<String,PackageDescriptor> packages,
                                     final String project) {
                super(id);
                this.strroot = root.toString();
                this.nameFormat = "%s_" + packageName + "." + suffix;
                this.folder = packages.get(packageName).getFolder();
                this.project = project;
            }
            
            @Override
            protected void addPathsFor(final String id,
                                       final ImmutableList.Builder<Path> paths,
                                       final ImmutableMap.Builder<URI,IOException> failures) {
                final Path path = Paths.get(strroot, packageSubdirs.getSubDir(project), id, folder, String.format(nameFormat, id));
                logger.trace("downloading {}: {}", id, path);
                final String strpath = path.toString();
                final PackageFile pkgf;
                if (pfCache.containsKey(id)) {
                	final Map<String,PackageFile> fileMap = Collections.synchronizedMap(pfCache.get(id));
                	if (fileMap.containsKey(strpath)) {
                		pkgf = fileMap.get(strpath);
                	} else {
                		pkgf = XDAT.getContextService().getBean(PackageFileService.class).getPackageFileByPath(strpath);
                		synchronized (fileMap) {
                			if (!fileMap.containsKey(strpath)) {
                				fileMap.put(strpath,pkgf);
                			}
                		}
                	}
                } else {
                	pkgf = XDAT.getContextService().getBean(PackageFileService.class).getPackageFileByPath(strpath);
                	final Map<String,PackageFile> newIdMap = Collections.synchronizedMap(new HashMap<String,PackageFile>()); 
           			newIdMap.put(strpath, pkgf);
                	synchronized (pfCache) {
                		if (!pfCache.containsKey(id)) {
                			pfCache.put(id, newIdMap);
                		}
                	}
                }
                if (null != pkgf) { // file exists
                    relativizeAndAdd(paths, path,
                                     pkgf.getFileCount(),
                                     pkgf.getFileSize());
                    final Path md5 = Paths.get(strpath + MD5_SUFFIX);
                    if (Files.exists(md5)) {
                        relativizeAndAdd(paths, md5);
                    }
                } else {
                    failures.put(path.toUri(),
                                 new FileNotFoundException(strpath));
                }
            }
        }

        public HCPZipFilePackageBuilder(final Path root, final String packageName, final String suffix) {
            super(root);
            this.packageName = packageName;
            this.suffix = suffix;
        }

        @Override
        public Package apply(final String project, final XDATUser user, final String id) {
            Throwable cause = null;
            try {
                if (user.canRead(SECURING_ELEMENT, project)) {
                    return new HCPZipFilePackage(getRoot(), id, packageName, suffix, _packages.get(project), project);
                }
            } catch (Throwable t) {
                cause = t;
            }
            throw new AccessDeniedException("user " + user.getUsername() + " cannot access project " + project, cause);
        }
        
    }
}
