package org.nrg.xnat.download;

import java.io.File;
import java.net.MalformedURLException;
import java.net.URI;
import java.net.URL;
import java.nio.file.Paths;
import java.util.List;
import java.util.Map;

import javax.inject.Inject;
import javax.inject.Named;
import javax.servlet.http.HttpServletResponse;

import org.json.JSONException;
import org.nrg.xdat.XDAT;
import org.nrg.xdat.om.XnatProjectdata;
import org.nrg.xdat.security.XDATUser;
import org.nrg.xnat.download.ResourceZipFilePackageBuilder.ResourceNotFoundException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.servlet.ModelAndView;

import com.google.common.base.Strings;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;

/**
 * @author Evan Haas Right now it just handles project-level resources; enhancing to support session-level resources
 *         shouldn't be too onerous. Ugly hack in request mapping to prevent Spring from truncating the file extension.
 */
@Controller
@RequestMapping("/download/projects/{PROJECT_ID}/resources/{RESOURCE_ID}/files/{FILE_PATH}/end")
public final class DownloadResourceService {
    public static final String DOWNLOAD_LOGGER_NAME = "org.nrg.xnat.download.monitor";

    public static final String MODEL_PATHS = "paths";
    public static final String MODEL_FAILURES = "failures";

    private final Logger logger = LoggerFactory.getLogger(DownloadResourceService.class);
    private final Logger downloadLogger = LoggerFactory.getLogger(DOWNLOAD_LOGGER_NAME);

    @Inject
    private URL asperaNodeURL;
    @Inject
    private String nodeUser;
    @Inject
    private String nodePassword;
    @Inject
    private String tokenEncryptionKey;
    @Inject
    private Integer target_rate_kbps;

    @Inject
    @Named("packageRoots")
    private PackageRoot packageRoots;

    @RequestMapping(method = RequestMethod.POST)
    public Object handlePost(final Map<String, Object> model,
            @PathVariable(value = "PROJECT_ID") final String projectId,
            @PathVariable(value = "RESOURCE_ID") final String resourceId,
            @PathVariable(value = "FILE_PATH") final String filePath,
            @RequestParam(value = "destination", required = false) final String destinationPath)
                    throws ProjectNotFoundException, ResourceNotFoundException, JSONException, MalformedURLException {

        logger.trace("Requesting download of resource {}, filepath {}", resourceId, filePath);

        final XDATUser user = XDAT.getUserDetails();
        final XnatProjectdata project = XnatProjectdata.getProjectByIDorAlias(projectId, user, false);
        if (null == project) {
            throw new ProjectNotFoundException();
        }
        try {
            if (!project.canRead(user)) {
                throw new AccessDeniedException("user " + user.getLogin() + " does not have access to project " + project);
            }
        } catch (Throwable t) {
            throw new AccessDeniedException("unable to verify access to " + project + " for user " + user.getLogin());
        }

        if (!filePath.endsWith(".zip")) {
            logger.error("resources must have .zip suffix");
            throw new ResourceNotFoundException();
        }

        final List<Package> packages = Lists.newArrayList();
        final Map<URI, Object> failures = Maps.newLinkedHashMap();
        // TODO this guy should probably be injected
        final PackageBuilder builder = new ResourceZipFilePackageBuilder(Paths.get(packageRoots.getRoot(projectId).getAbsolutePath()),
                project, resourceId);
        final Package pkg = builder.apply(projectId, user, filePath);
        long size = pkg.getSize();
        if (pkg.getPathIterator().hasNext()) {
            packages.add(pkg);
        }
        failures.putAll(pkg.getFailures());

        if (packages.isEmpty()) {
            logger.error("No matching resource {}:{}:{} found in {}",
                         new Object[]{ project.getName(),
                                       resourceId,
                                       filePath,
                                       packageRoots.getRoot(projectId).getAbsolutePath()});
            throw new ResourceNotFoundException();
        }

        model.put(MODEL_PATHS, packages);
        model.put(MODEL_FAILURES, failures);

        final Map<String, Object> transferRequestOptions = Maps.newLinkedHashMap();
        if (!Strings.isNullOrEmpty(destinationPath)) {
            transferRequestOptions.put(AsperaTokenView.OPT_DESTINATION_ROOT, destinationPath);
        }
        transferRequestOptions.put(AsperaTokenView.OPT_COOKIE, "XDATUser=" + user.getLogin());
        final Map<String, Object> transferSpecOptions = Maps.newLinkedHashMap();
        transferSpecOptions.put("target_rate_kbps", target_rate_kbps);

        downloadLogger.info("{} downloading {} from project {}, resource {} ({} bytes)", new Object[] {
                user.getLogin(), filePath, projectId, resourceId, size
        });
        return new ModelAndView(new AsperaTokenView(asperaNodeURL, nodeUser, nodePassword, transferRequestOptions,
                transferSpecOptions), model);
    }

    @ExceptionHandler(ProjectNotFoundException.class)
    @ResponseStatus(value = HttpStatus.NOT_FOUND, reason = "The project was not found")
    public void handleProjectNotFoundException(ProjectNotFoundException e, HttpServletResponse response) {
        logger.error("no matching projects found", e);
    }

    @ExceptionHandler(ResourceNotFoundException.class)
    @ResponseStatus(value = HttpStatus.NOT_FOUND, reason = "The resource/file was not found")
    public void handleResourceNotFoundException(ResourceNotFoundException e, HttpServletResponse response) {
        logger.error("no matching resources found", e);
    }
}
