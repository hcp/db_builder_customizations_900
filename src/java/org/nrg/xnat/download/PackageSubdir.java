/**
 * Copyright (c) 2013 Washington University School of Medicine
 */
package org.nrg.xnat.download;

import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableMap;
import org.nrg.xdat.XDAT;
import org.nrg.xdat.security.XDATUser;
import org.nrg.xnat.entities.PackageFile;
import org.nrg.xnat.services.PackageFileService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.access.AccessDeniedException;

import javax.inject.Inject;
import javax.inject.Named;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.URI;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.*;

public class PackageSubdir {

    public LinkedHashMap<String, String> getSubDirs() {
        return _subDirs;
    }

    public void setSubDirs(LinkedHashMap<String, String> subDirs) {
        _subDirs = subDirs;
    }

    public String getSubDir(String project) {
        return _subDirs.get(project);
    }

    private LinkedHashMap<String, String> _subDirs = new LinkedHashMap<String, String>();

 }
