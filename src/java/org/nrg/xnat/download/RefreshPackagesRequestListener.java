package org.nrg.xnat.download;

import org.nrg.xdat.XDAT;
import org.nrg.xnat.entities.PackageFile;
import org.nrg.xnat.services.PackageFileService;
import org.nrg.xnat.utils.ArchiveMetaData;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.IOException;
import java.nio.file.FileVisitResult;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.SimpleFileVisitor;
import java.nio.file.attribute.BasicFileAttributes;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: mmckay01
 * Date: 11/6/13
 * Time: 4:23 PM
 * To change this template use File | Settings | File Templates.
 */
public class RefreshPackagesRequestListener {

    private final static Logger logger = LoggerFactory.getLogger(RefreshPackagesRequestListener.class);

    public void onRefreshPackagesRequest(final RefreshPackagesRequest refreshPackagesRequest) throws Exception {
        final PackageFileService service = XDAT.getContextService().getBean(PackageFileService.class);
        final List<String> filePaths = new ArrayList<String>();
        try {
        	// Add packages not currently represented in the database
            Files.walkFileTree(refreshPackagesRequest.getPackageDir(), new SimpleFileVisitor<Path>() {
                @Override
                public FileVisitResult visitFile(Path file, BasicFileAttributes attrs) 
                        throws IOException {
                	final String packageFilePath = file.toString();
                    if (packageFilePath.endsWith(".zip")) {
                    	filePaths.add(packageFilePath);
                        final PackageFile existingFile = service.getPackageFileByPath(packageFilePath);
                        final File currFile = new File(file.toUri());
                        long lastModified = currFile.lastModified();
                        if (existingFile == null || lastModified != existingFile.getLastModified()) {
                            //File has changed since its entry was updated in the database or is not yet in the database.
                            long newSize = currFile.length();
                            int newCount = 1;
                            try {
                                newCount = ArchiveMetaData.getArchiveMap(packageFilePath).get(ArchiveMetaData.EntryType.File).size();
                            } catch (Exception e) {
                                logger.info("Exception on get count:" + packageFilePath, e);
                            }
                            if (existingFile == null) {
                            	String packageDir = refreshPackagesRequest.getPackageDir().toString();
                            	if (packageFilePath.startsWith(packageDir)) {
                            		final String[] filePathParts = packageFilePath.replaceFirst(packageDir + "[/]*","").split("[/]+");
                            		final String subjectPart = filePathParts[0];
                            		if (!filePathParts[filePathParts.length-1].startsWith(subjectPart)) {
                            			logger.error("ERROR:  File name unexpectedly does not begin with subject label (subjectPart=" + subjectPart + ", packageFilePath=" + packageFilePath + ").  File will be skipped");
                            			return FileVisitResult.CONTINUE;
                            		}
                            		final String packagePart = filePathParts[filePathParts.length-1].replaceFirst(subjectPart + "_", "").replaceFirst(".zip$", "");
                            		final PackageFile newFile = new PackageFile(packageDir , subjectPart, packagePart, packageFilePath, newSize, newCount, lastModified);
                            		XDAT.getContextService().getBean(PackageFileService.class).create(newFile);
                            	} else {
                            		logger.error("ERROR:  File path unexpectedly does not begin with package directory (packageDir=" + packageDir + ", packageFilePath=" + packageFilePath + ").  File will be skipped");
                            	}
                            } else {
                                if (existingFile.getFileCount() != newCount || existingFile.getFileSize() != newSize) {
                                    existingFile.setFileCount(newCount);
                                    existingFile.setFileSize(newSize);
                                    existingFile.setLastModified(lastModified);
                                    XDAT.getContextService().getBean(PackageFileService.class).update(existingFile);
                                }
                            }
                        }
                    }
                    return FileVisitResult.CONTINUE;
                }
            });
            // Remove entries for which files no longer exist
            for (PackageFile pf : service.getAll()) {
            	if (pf.getFilePath().indexOf(refreshPackagesRequest.getPackageDir().toString())==0 && !filePaths.contains(pf.getFilePath())) {
            		service.delete(pf);
            	}
            }
            service.refreshCache();
            XDAT.getContextService().getBean(HCPPackageBuilderProvider.class).refreshPackageFileCache();
        } catch (IOException e){
            if(refreshPackagesRequest.getSubDir()!=null){
                logger.error("IO Exception when updating the package information table for "+refreshPackagesRequest.getSubDir()+".", e);
            }
            else{
                logger.error("IO Exception when updating the package information table.", e);
            }
        } catch (Exception e){
            if(refreshPackagesRequest.getSubDir()!=null){
                logger.error("Exception updating the package information table for "+refreshPackagesRequest.getSubDir()+".", e);
            }
            else{
                logger.error("Exception updating the package information table.", e);
            }
        }
    } 
    
}
