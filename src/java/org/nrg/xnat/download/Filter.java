package org.nrg.xnat.download;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import javax.xml.bind.annotation.XmlID;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.Set;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Created by mmckay01 on 5/12/14.
 */
@XmlRootElement
public class Filter {
    private final Logger logger = LoggerFactory.getLogger(DownloadPackageService.class);

    @XmlID
    public String getName() {
        return _name;
    }

    public void setName(String _name) {
        this._name = _name;
    }

    public String getLabel() {
        return _label;
    }

    public void setLabel(String _label) {
        this._label = _label;
    }

    public String getType() {
        return _type;
    }

    public void setType(String _type) {
        this._type = _type;
    }

    public List<Option> getOptions() {
        return _options;
    }

    public void setOptions(List<Option> _options) {
        this._options = _options;
    }

    public JSONObject toJSON() {
        final JSONObject filter = new JSONObject();
        try{
            if(_name!=null){
                filter.put("name", _name);
            }
            if(_label!=null){
                filter.put("label", _label);
            }
            if(_type!=null){
                filter.put("type", _type);
            }

            JSONArray filter_options = new JSONArray();
            if(_options!=null){
                for(Option option:_options){
                    filter_options.put(option.toJSON());
                }
            }
            if(filter_options.length()>0){
                filter.put("options", filter_options);
            }
        }
        catch(JSONException e){
            logger.error("Exception creating filter JSON for "+_name+".", e);
        }
        return filter;
    }

    private String _name = null;
    private String _label = null;
    private String _type = null;
    private List<Option> _options= null;
}
