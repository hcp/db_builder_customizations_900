package org.nrg.xnat.download;

import javax.xml.bind.annotation.XmlRootElement;
import java.util.LinkedHashMap;


/**
 * Created by mmckay01 on 5/12/14.
 */
@XmlRootElement
public class FilterMap {

    public LinkedHashMap<String, Filter> getMap() {
        return _map;
    }

    public void setMap(LinkedHashMap<String, Filter> map) {
        _map = map;
    }

    public Filter getFilter(String project) {
        return _map.get(project);
    }

    private LinkedHashMap<String, Filter> _map = new LinkedHashMap<String, Filter>();
}
