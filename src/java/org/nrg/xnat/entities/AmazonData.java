package org.nrg.xnat.entities;

/**
 * Created by mmckay01 on 1/29/14.
 */

import org.apache.commons.lang.StringUtils;
import org.nrg.framework.orm.hibernate.AbstractHibernateEntity;

import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

@Entity
@Table(uniqueConstraints = @UniqueConstraint(columnNames = {"userLogin","awsGroup"}))
public class AmazonData extends AbstractHibernateEntity{

    private String userLogin;
    private boolean hasAws;
    private String accessKey;
    private String awsGroup;

    public AmazonData() {
    }

    public AmazonData(String userLogin, boolean hasAws, String accessKey, String awsGroup) {
        this.userLogin=userLogin;
        this.hasAws = hasAws;
        this.accessKey = accessKey;
        this.awsGroup = awsGroup;
    }

    public String getUserLogin(){
        return userLogin;
    }

    public boolean getHasAws(){
        return hasAws;
    }

    public String getAccessKey(){
        return accessKey;
    }

    public String getAwsGroup(){
        return awsGroup;
    }

    public void setUserLogin(String userLogin){
        this.userLogin = userLogin;
    }

    public void setHasAws(boolean hasAws){
        this.hasAws = hasAws;
    }

    public void setAccessKey(String accessKey){
        this.accessKey = accessKey;
    }

    public void setAwsGroup(String awsGroup){
        this.awsGroup = awsGroup;
    }

    @Override
    public boolean equals(Object object) {
        if (object == null || !(object instanceof AmazonData)) {
            return false;
        }
        AmazonData other = (AmazonData) object;
        return           (StringUtils.equals(getUserLogin(),other.getUserLogin()) &&
                StringUtils.equals(getAccessKey(),other.getAccessKey()) &&
                StringUtils.equals(getAwsGroup(),other.getAwsGroup()) &&
                getHasAws()==other.getHasAws());
    }


}
