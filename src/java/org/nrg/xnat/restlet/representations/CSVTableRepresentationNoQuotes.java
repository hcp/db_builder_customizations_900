// Copyright 2010 Washington University School of Medicine All Rights Reserved
package org.nrg.xnat.restlet.representations;

import org.apache.log4j.Logger;
import org.nrg.xft.XFTTable;
import org.nrg.xft.utils.StringUtils;
import org.restlet.data.MediaType;
import org.restlet.resource.OutputRepresentation;

import java.io.*;
import java.util.Hashtable;
import java.util.Map;

public class CSVTableRepresentationNoQuotes extends OutputRepresentation {
    static Logger logger = Logger.getLogger(CSVTableRepresentationNoQuotes.class);
	XFTTable table = null;
	Hashtable<String,Object> tableProperties = null;
	Map<String,Map<String,String>> cp=new Hashtable<String,Map<String,String>>();
	
	public CSVTableRepresentationNoQuotes(XFTTable table, MediaType mediaType) {
		super(mediaType);
		this.table=table;
	}
	
	public CSVTableRepresentationNoQuotes(XFTTable table, Hashtable<String, Object> metaFields, MediaType mediaType) {
		super(mediaType);
		this.table=table;
		this.tableProperties=metaFields;
	}
	
	public CSVTableRepresentationNoQuotes(XFTTable table, Map<String, Map<String, String>> columnProperties, Hashtable<String, Object> metaFields, MediaType mediaType) {
		super(mediaType);
		this.table=table;
		this.tableProperties=metaFields;
		if(columnProperties!=null)this.cp=columnProperties;
	}

	@Override
	public void write(OutputStream os) throws IOException {
		OutputStreamWriter sw = new OutputStreamWriter(os);
		BufferedWriter writer = new BufferedWriter(sw);
        convertToCSV(writer, this.cp, null);
	    writer.flush();
	    
	}

    public void convertToCSV(Writer w,Map<String,Map<String,String>> columnProperties,String title){
        try {
            Writer writer = new BufferedWriter(w);
            for (int i=0;i<table.getNumCols();i++)
            {
                if(i>0)
                    writer.write(",");
                writer.write(table.getColumns()[i]);
            }
            writer.write("\n");
            writer.flush();

            for (Object[] row:table.rows())
            {
                for (int i=0;i<table.getNumCols();i++)
                {
                    if(i>0)
                        writer.write(",");
                    if(null!=row[i]){
                        writer.write(StringUtils.ReplaceStr(StringUtils.ReplaceStr(ValueParser(row[i]), "\"", "'"), "," , ";"));
                    }
                }
                writer.write("\n");
                writer.flush();
            }
            writer.flush();
        } catch (IOException e) {
            logger.error(e);
        }
    }

    public static String ValueParser(Object o)
    {
        if (o != null)
        {
            if (o.getClass().getName().equalsIgnoreCase("[B"))
            {
                byte[] b = (byte[]) o;
                ByteArrayOutputStream baos = new ByteArrayOutputStream();
                try {
                    baos.write(b);
                } catch (IOException e) {
                    e.printStackTrace();
                }
                return baos.toString();
            }
            return o.toString();
        }else
        {
            return "";
        }
    }

}