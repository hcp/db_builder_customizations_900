package org.nrg.xnat.restlet.projectList.filters;

import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.nrg.action.ClientException;
import org.nrg.action.ServerException;
import org.nrg.xdat.search.DisplayCriteria;
import org.nrg.xdat.security.XDATUser;
import org.nrg.xft.search.CriteriaCollection;
import org.nrg.xnat.restlet.resources.ProjectListResource.ProjectListFilterI;
import org.restlet.data.Status;

public class ConnectomeTypeFilter implements ProjectListFilterI {

	@Override
	public void applyFilter(final Map<String,String> params, CriteriaCollection andCC, CriteriaCollection orCC, XDATUser user) throws ServerException,ClientException {
		if(params==null || !params.containsKey("cType")){
			//don't include projects with a type
			try {
				DisplayCriteria dc = new DisplayCriteria();
				dc.setSearchFieldByDisplayField("xnat:projectData","HCP_TYPE_VALUE");
				dc.setComparisonType(" IS ");
				dc.setValue(" NULL ",false);
				dc.setOverrideDataFormatting(true);
				andCC.addCriteria(dc);
			} catch (Throwable e) {
				throw new ClientException(Status.CLIENT_ERROR_UNPROCESSABLE_ENTITY, e);
			}
		} else if(StringUtils.equals(params.get("cType"), "ALL")){
			//show all
		}else{
			try {
				//retrieve only matching types
				DisplayCriteria dc = new DisplayCriteria();
				dc.setSearchFieldByDisplayField("xnat:projectData","HCP_TYPE_VALUE");
				dc.setComparisonType("=");
				dc.setValue(params.get("cType"),false);
				andCC.addCriteria(dc);
				// Only return matches for the current project context
				DisplayCriteria dc2 = new DisplayCriteria();
				dc2.setSearchFieldByDisplayField("xnat:projectData","ROOT_PROJECT");
				dc2.setComparisonType("=");
				dc2.setValue(params.get("umbrella"), false);
				andCC.addCriteria(dc2);
			} catch (Throwable e) {
				throw new ClientException(Status.CLIENT_ERROR_UNPROCESSABLE_ENTITY, e);
			}
		}
	}

	@Override
	public boolean match(Map<String, String> params) {
		return true;
	}

}
