package org.nrg.xnat.restlet.extensions;

import java.io.IOException;
import java.net.URLDecoder;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Hashtable;
import org.apache.commons.lang.StringUtils;
import org.apache.velocity.VelocityContext;
import org.nrg.xdat.XDAT;
import org.nrg.xdat.security.XDATUser;
import org.nrg.xdat.turbine.utils.AdminUtils;
import org.nrg.xnat.entities.datadictionary.Attribute;
import org.nrg.xnat.restlet.XnatRestlet;
import org.nrg.xnat.restlet.resources.SecureResource;
import org.apache.commons.lang.StringUtils;
import org.restlet.Context;
import org.restlet.data.MediaType;
import org.restlet.data.Request;
import org.restlet.data.Response;
import org.restlet.data.Status;
import org.restlet.resource.Representation;
import org.restlet.resource.ResourceException;
import org.restlet.resource.StringRepresentation;
import org.restlet.resource.Variant;
import org.nrg.xnat.services.unrelatedsubjects.UnrelatedSubjectsService;
import org.nrg.xft.XFTTable;
import org.nrg.xnat.restlet.representations.HTMLTableRepresentation;

/**
 * Created with IntelliJ IDEA.
 * User: mmckay01
 * Date: 8/13/13
 * Time: 3:18 PM
 * To change this template use File | Settings | File Templates.
 */
@XnatRestlet({"/services/unrelated"})
public class UnrelatedSubjectsRestlet extends SecureResource{
    private static final String URL_ENCODING = "UTF-8";
    private final UnrelatedSubjectsService _service;



    public UnrelatedSubjectsRestlet(Context context, Request request, Response response) throws Exception {
        super(context, request, response);
        this.getVariants().add(new Variant(MediaType.ALL));

        _service = XDAT.getContextService().getBean(UnrelatedSubjectsService.class);
    }

    @Override
    public Representation represent(Variant variant) throws ResourceException {
        XFTTable table = new XFTTable();
        try {
            if (getHttpServletRequest().getSession().getAttribute("tier") != null &&
                    (Integer)getHttpServletRequest().getSession().getAttribute("tier") > 0) {

                ArrayList<String> columnHeaders = new ArrayList<String>();
                columnHeaders.add("Subject");

                int maxFamilySize = _service.getMaxFamilySize();
                for(int count = 1;count<maxFamilySize;count++){
                    columnHeaders.add("Sibling "+count);
                }
                table.initTable(columnHeaders, _service.getSubjectLists());

                table.toHTML(true,"FFFFFF","FFFFCC",new java.util.Hashtable(),0);
            }
        }
        catch(Exception e){

        }
        return new HTMLTableRepresentation(table, null, new Hashtable<String, Object>(), MediaType.TEXT_HTML, true);
    }

}
