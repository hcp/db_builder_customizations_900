/*
 * AmazonRestlet
 * Copyright (c) 2014. Washington University School of Medicine
 * All Rights Reserved
 *
 * Released under the Simplified BSD License
 */

package org.nrg.xnat.restlet.extensions;

import java.io.IOException;
import java.net.URLDecoder;
import java.util.ArrayList;
import java.util.List;
import com.amazonaws.auth.*;
import com.amazonaws.services.identitymanagement.*;
import com.amazonaws.services.identitymanagement.model.*;
import org.apache.commons.lang.StringUtils;
import org.apache.velocity.VelocityContext;
import org.nrg.xdat.XDAT;
import org.nrg.xdat.turbine.utils.AdminUtils;
import org.nrg.xnat.download.ProjectNotFoundException;
import org.nrg.xnat.entities.datadictionary.Attribute;
import org.nrg.xnat.restlet.XnatRestlet;
import org.nrg.xnat.restlet.resources.SecureResource;
import org.restlet.Context;
import org.restlet.data.MediaType;
import org.restlet.data.Request;
import org.restlet.data.Response;
import org.restlet.data.Status;
import org.restlet.resource.Representation;
import org.restlet.resource.ResourceException;
import org.restlet.resource.StringRepresentation;
import org.restlet.resource.Variant;
import org.nrg.xdat.security.XDATUser;
import org.nrg.xdat.om.XnatProjectdata;
import org.nrg.xnat.entities.AmazonData;
import org.nrg.xnat.services.amazonData.AmazonDataService;
/**
 * AmazonRestlet
 *
 * @author mmckay01
 * @since 12/9/14
 */
@XnatRestlet({"/services/amazon/{ACTION}"})
public class AmazonRestlet extends SecureResource {

    private static final String URL_ENCODING = "UTF-8";
    public AmazonRestlet(Context context, Request request, Response response) throws Exception {
        super(context, request, response);
        this.getVariants().add(new Variant(MediaType.ALL));

        _action = URLDecoder.decode(StringUtils.defaultString((String) getRequest().getAttributes().get("ACTION")),
                URL_ENCODING);

    }

    public boolean canModifyAWS() {
        boolean canModify = false;
        try{
            XDATUser user = XDAT.getUserDetails();
            //XnatProjectdata project = XnatProjectdata.getProjectByIDorAlias((String)getHttpSession().getAttribute("defaultProject"), user, false);
            if ((user.canRead("xnat:subjectData/project",(String)getHttpSession().getAttribute("defaultProject"))) || (user.isSiteAdmin())) {
                canModify = true;
            }
        }
        catch(Exception e){
            logger.error("Error checking whether user has signed DUT.",e);
        }
        return canModify;
    }

    @Override
    public Representation represent(Variant variant) throws ResourceException {
        String accessKey = XDAT.getContextService().getBean(AmazonDataService.class).getKey();
        String secretKey = XDAT.getContextService().getBean(AmazonDataService.class).getSecretKey();
        String allUsers = XDAT.getContextService().getBean(AmazonDataService.class).getAllUsers();
        String group = XDAT.getContextService().getBean(AmazonDataService.class).getAwsGroup();
        if (_action.equals("new")) {
            if(!canModifyAWS()){
                logger.error("User must have signed DUT before getting AWS access.");
                return new StringRepresentation("User must have signed DUT before getting AWS access.");
            }
            BasicAWSCredentials awsCreds = new BasicAWSCredentials(accessKey,secretKey);
            AmazonIdentityManagementClient aClient = new AmazonIdentityManagementClient(awsCreds);


            //AmazonIdentityManagementClient aClient = new AmazonIdentityManagementClient();
            try {
                String usName = user.getUsername();
                CreateUserRequest uReq = new com.amazonaws.services.identitymanagement.model.CreateUserRequest(usName);
                CreateAccessKeyRequest key = new CreateAccessKeyRequest();
                key.withUserName(usName);
                key.setRequestCredentials(awsCreds);
                uReq.setRequestCredentials(key.getRequestCredentials());
                uReq.setPath("/");

                AmazonIdentityManagementClient client = new AmazonIdentityManagementClient(awsCreds);
                CreateUserResult newAUserResult = client.createUser(uReq);

				AddUserToGroupRequest addToAllUsersRequest = new AddUserToGroupRequest(allUsers,usName);
                client.addUserToGroup(addToAllUsersRequest);
				
                AddUserToGroupRequest addToGroupRequest = new AddUserToGroupRequest(group,usName);
                client.addUserToGroup(addToGroupRequest);

                //CreateUserResult newAUserResult = aClient.createUser(uReq);
            } catch (Throwable e){
                logger.error("Error during AWS account setup.", e);
                return new StringRepresentation(e.getMessage());
            }

            CreateAccessKeyResult newAKeyResult = aClient.createAccessKey(new CreateAccessKeyRequest(user.getUsername()));
            AccessKey newAKey = newAKeyResult.getAccessKey();
            String aUserName = newAKey.getUserName();
            String aStatus = newAKey.getStatus();
            String aAccessKeyId = newAKey.getAccessKeyId();
            String aSecretAccessKey = newAKey.getSecretAccessKey();

            AmazonData newData = new AmazonData(aUserName, true, aAccessKeyId, allUsers);
			AmazonData newDataForGroup = new AmazonData(aUserName, true, aAccessKeyId, group);
			
            try {
                XDAT.getContextService().getBean(AmazonDataService.class).create(newData);
                XDAT.getContextService().getBean(AmazonDataService.class).create(newDataForGroup);
            } catch (Throwable ex) {
                logger.error("Exception storing Amazon credential information in database.", ex);
                return new StringRepresentation(ex.getMessage());
            }
            getHttpSession().setAttribute("hasAws", true);
            getHttpSession().setAttribute("awsAccessKey", aAccessKeyId);
            if (aStatus.equals("Active")) {
                String info = aUserName + "," + aAccessKeyId + "," + aSecretAccessKey;
                return new StringRepresentation(info);
            }
        }
        else if (_action.equals("delete")) {
            BasicAWSCredentials awsCreds = new BasicAWSCredentials(accessKey,secretKey);
            AmazonIdentityManagementClient aClient = new AmazonIdentityManagementClient(awsCreds);
            String usName = user.getUsername();
            if(user.isSiteAdmin()) {
                String userToDelete = SecureResource.getQueryVariable("userToDelete", getRequest());
                if(userToDelete!=null&&!(userToDelete.equals(""))){
                    usName=userToDelete;
                }
            }
            //AmazonIdentityManagementClient aClient = new AmazonIdentityManagementClient();
            try {
                DeleteUserRequest uReq = new com.amazonaws.services.identitymanagement.model.DeleteUserRequest(usName);

                AmazonIdentityManagementClient client = new AmazonIdentityManagementClient(awsCreds);

                ListAccessKeysRequest keyReq = new ListAccessKeysRequest();
                keyReq.setUserName(usName);
                ListAccessKeysResult re = client.listAccessKeys(keyReq);
                List<AccessKeyMetadata> userKeys = re.getAccessKeyMetadata();
                for(AccessKeyMetadata key : userKeys){
                    DeleteAccessKeyRequest delReq = new DeleteAccessKeyRequest(key.getUserName(),key.getAccessKeyId());
                    client.deleteAccessKey(delReq);
                }

                ListGroupsForUserRequest grReq = new ListGroupsForUserRequest(usName);
                ListGroupsForUserResult grRes = client.listGroupsForUser(grReq);
                List<Group> userGroups = grRes.getGroups();
                for(Group userGroup : userGroups){
                    RemoveUserFromGroupRequest remGrReq = new RemoveUserFromGroupRequest(userGroup.getGroupName(),usName);
                    client.removeUserFromGroup(remGrReq);
                }


                client.deleteUser(uReq);

                //CreateUserResult newAUserResult = aClient.createUser(uReq);

            } catch (Exception e) {
                logger.error("Error deleting AWS account for this user.", e);
                return new StringRepresentation(e.getMessage());
            }
            try{
                List<AmazonData> dataInTable = XDAT.getContextService().getBean(AmazonDataService.class).getAmazonDataForUser(usName);
                if(dataInTable!=null && dataInTable.size()>0){
                    for(AmazonData amData:dataInTable){
                        try {
                            XDAT.getContextService().getBean(AmazonDataService.class).delete(amData);
                        } catch (Throwable ex) {
                            logger.error("Exception removing Amazon credential information from database.", ex);
                            return new StringRepresentation(ex.getMessage());
                        }
                    }
                }
            }catch(Throwable e){
                logger.error("Exception getting Amazon credential information from the database.",e);
                return new StringRepresentation(e.getMessage());
            }
            getHttpSession().setAttribute("hasAws", false);
            getHttpSession().setAttribute("awsAccessKey", "");
            return new StringRepresentation("Amazon credentials for "+usName+" have been deleted.");
        }

        else if (_action.equals("hasAws")) {
            try{
                List<AmazonData> dataInTable = XDAT.getContextService().getBean(AmazonDataService.class).getAmazonDataForUser(user.getUsername());
                if(dataInTable==null || dataInTable.size()<1){
                    return new StringRepresentation("false");
                }
                else{
                    return new StringRepresentation("true");
                }
            }catch(Throwable e){
                logger.error("Exception getting Amazon credential information from the database.",e);
            }
        }
        else if (_action.equals("reset")) {
            if(!canModifyAWS()){
                logger.error("User must have signed DUT before getting AWS access.");
                return new StringRepresentation("User must have signed DUT before getting AWS access.");
            }
            BasicAWSCredentials awsCreds = new BasicAWSCredentials(accessKey,secretKey);
            AmazonIdentityManagementClient aClient = new AmazonIdentityManagementClient(awsCreds);

            //AmazonIdentityManagementClient aClient = new AmazonIdentityManagementClient();
            try {
                String usName = user.getUsername();
                DeleteUserRequest uReq = new com.amazonaws.services.identitymanagement.model.DeleteUserRequest(usName);

                AmazonIdentityManagementClient client = new AmazonIdentityManagementClient(awsCreds);


                ListAccessKeysRequest keyReq = new ListAccessKeysRequest();
                keyReq.setUserName(usName);
                ListAccessKeysResult re = client.listAccessKeys(keyReq);
                List<AccessKeyMetadata> userKeys = re.getAccessKeyMetadata();
                for(AccessKeyMetadata key : userKeys){
                    DeleteAccessKeyRequest delReq = new DeleteAccessKeyRequest(key.getUserName(),key.getAccessKeyId());
                    client.deleteAccessKey(delReq);
                }

                ListGroupsForUserRequest grReq = new ListGroupsForUserRequest(usName);
                ListGroupsForUserResult grRes = client.listGroupsForUser(grReq);
                List<Group> userGroups = grRes.getGroups();
                for(Group userGroup : userGroups){
                    RemoveUserFromGroupRequest remGrReq = new RemoveUserFromGroupRequest(userGroup.getGroupName(),usName);
                    client.removeUserFromGroup(remGrReq);
                }


                client.deleteUser(uReq);
            } catch (Exception e) {
                logger.error("Error deleting AWS account for this user.", e);
                return new StringRepresentation(e.getMessage());
            }
            try{
                List<AmazonData> dataInTable = XDAT.getContextService().getBean(AmazonDataService.class).getAmazonDataForUser(user.getUsername());
                if(dataInTable!=null && dataInTable.size()>0){
                    for(AmazonData amData:dataInTable){
                        try {
                            XDAT.getContextService().getBean(AmazonDataService.class).delete(amData);
                        } catch (Throwable ex) {
                            logger.error("Exception removing Amazon credential information from database.", ex);
                            return new StringRepresentation(ex.getMessage());
                        }
                    }
                }
            }catch(Throwable e){
                logger.error("Exception getting Amazon credential information from the database.",e);
                return new StringRepresentation(e.getMessage());
            }



            //AmazonIdentityManagementClient aClient = new AmazonIdentityManagementClient();
            try {
                String usName = user.getUsername();
                CreateUserRequest uReq = new com.amazonaws.services.identitymanagement.model.CreateUserRequest(usName);
                CreateAccessKeyRequest key = new CreateAccessKeyRequest();
                key.withUserName(usName);
                key.setRequestCredentials(awsCreds);
                uReq.setRequestCredentials(key.getRequestCredentials());
                uReq.setPath("/");

                AmazonIdentityManagementClient client = new AmazonIdentityManagementClient(awsCreds);
                CreateUserResult newAUserResult = client.createUser(uReq);

                AddUserToGroupRequest addToAllUsersRequest = new AddUserToGroupRequest(allUsers,usName);
                client.addUserToGroup(addToAllUsersRequest);

                AddUserToGroupRequest addToGroupRequest = new AddUserToGroupRequest(group,usName);
                client.addUserToGroup(addToGroupRequest);
                //CreateUserResult newAUserResult = aClient.createUser(uReq);
            } catch (Throwable e){
                logger.error("Error during AWS account setup.", e);
                return new StringRepresentation(e.getMessage());
            }
            CreateAccessKeyResult newAKeyResult = aClient.createAccessKey(new CreateAccessKeyRequest(user.getUsername()));
            AccessKey newAKey = newAKeyResult.getAccessKey();
            String aUserName = newAKey.getUserName();
            String aStatus = newAKey.getStatus();
            String aAccessKeyId = newAKey.getAccessKeyId();
            String aSecretAccessKey = newAKey.getSecretAccessKey();

            AmazonData newData = new AmazonData(aUserName, true, aAccessKeyId, allUsers);
			AmazonData newDataForGroup = new AmazonData(aUserName, true, aAccessKeyId, group);
			
            try {
                XDAT.getContextService().getBean(AmazonDataService.class).create(newData);
                XDAT.getContextService().getBean(AmazonDataService.class).create(newDataForGroup);
            } catch (Throwable ex) {
                logger.error("Exception storing Amazon credential information in database.", ex);
                return new StringRepresentation(ex.getMessage());
            }
            getHttpSession().setAttribute("hasAws", true);
            getHttpSession().setAttribute("awsAccessKey", aAccessKeyId);
            if (aStatus.equals("Active")) {
                String info = aUserName + "," + aAccessKeyId + "," + aSecretAccessKey;
                return new StringRepresentation(info);
            }

        }
        else {
            throw new ResourceException(Status.CLIENT_ERROR_BAD_REQUEST, "Unknown attribute action: " + _action);
        }
        return null;
    }

    private final String _action;
}
