package org.nrg.xnat.restlet.extensions;

import org.apache.commons.lang.StringUtils;
import org.apache.velocity.VelocityContext;
import org.nrg.xdat.XDAT;
import org.nrg.xdat.turbine.utils.AdminUtils;
import org.nrg.xnat.entities.Publication;
import org.nrg.xnat.restlet.XnatRestlet;
import org.nrg.xnat.restlet.resources.SecureResource;
import org.nrg.xnat.services.publication.PublicationService;
import org.restlet.Context;
import org.restlet.data.MediaType;
import org.restlet.data.Request;
import org.restlet.data.Response;
import org.restlet.data.Status;
import org.restlet.resource.Representation;
import org.restlet.resource.ResourceException;
import org.restlet.resource.StringRepresentation;
import org.restlet.resource.Variant;
import org.nrg.xdat.om.XnatProjectdata;
import org.apache.log4j.Logger;
import java.io.IOException;
import java.net.URLDecoder;
import java.util.ArrayList;
import java.util.List;

/**
 * RemovePublicationRestlet
 *
 * @author rherri01
 * @since 3/5/2014
 */
@XnatRestlet({"/services/removePublication"})
public class RemovePublicationRestlet extends SecureResource {
    private final PublicationService requests = XDAT.getContextService().getBean(PublicationService.class);
    static Logger logger = Logger.getLogger(RemovePublicationRestlet.class);

    public RemovePublicationRestlet(Context context, Request request, Response response) throws Exception{
        super(context, request, response);
        this.getVariants().add(new Variant(MediaType.ALL));
    }

    @Override public boolean allowDelete() { return false; }
    @Override public boolean allowPut()    { return true; }
    @Override public boolean allowGet()    { return false; }
    @Override public boolean allowPost()   { return false;  }

    @Override public void handlePut(){
        String pmid = SecureResource.getQueryVariable("pmid", getRequest());
        String project = SecureResource.getQueryVariable("project", getRequest());


        if((pmid != null) && (project != null)){

            try{
                XnatProjectdata proj = XnatProjectdata.getProjectByIDorAlias(project, user, false);
                if(!(user.canEdit(proj))){
                    throw new Exception("User does not have permission to edit this project.");
                }
                else{
                    List<Publication> oldPublications = XDAT.getContextService().getBean(PublicationService.class).getPublicationsByIdAndProject(pmid, project);
                    for(Publication oldPublication : oldPublications){
                        XDAT.getContextService().getBean(PublicationService.class).delete(oldPublication);
                    }
                }
            }
            catch(Throwable e) {
                logger.error("Exception removing publication.",e);
            }
        }
        else{
            logger.warn("Invalid PMID or project.");
        }
    }
}
