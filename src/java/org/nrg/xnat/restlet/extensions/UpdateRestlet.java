package org.nrg.xnat.restlet.extensions;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.nrg.xdat.XDAT;
import org.nrg.xnat.entities.Publication;
import org.nrg.xnat.entities.Update;
import org.nrg.xnat.restlet.XnatRestlet;
import org.nrg.xdat.om.XnatProjectdata;
import org.nrg.xnat.restlet.resources.SecureResource;
import org.nrg.xnat.services.publication.PublicationService;
import org.nrg.xnat.services.update.UpdateService;
import org.restlet.Context;
import org.restlet.data.MediaType;
import org.restlet.data.Request;
import org.restlet.data.Response;
import org.restlet.resource.Representation;
import org.restlet.resource.StringRepresentation;
import org.restlet.resource.Variant;

import java.io.IOException;
import java.util.Iterator;
import java.util.List;

@XnatRestlet({"/services/update"})
public class UpdateRestlet extends SecureResource {
    private final UpdateService requests = XDAT.getContextService().getBean(UpdateService.class);
    static Logger logger = Logger.getLogger(UpdateRestlet.class);

    public UpdateRestlet(Context context, Request request, Response response) throws Exception {
        super(context, request, response);
        this.getVariants().add(new Variant(MediaType.ALL));
    }

    @Override public boolean allowDelete() { return true; }
    @Override public boolean allowPut()    { return true; }
    @Override public boolean allowGet()    { return true; }
    @Override public boolean allowPost()   { return false;  }

    @Override public void handleGet(){
        try {
            List<Update> updates = null;
            String project = SecureResource.getQueryVariable("project", getRequest());
            if (!StringUtils.isBlank(project)) {
                updates = requests.getUpdatesForProject(project);
            }
            else{
                updates = requests.getUpdates();
            }

            Iterator<Update> updatesIterator = updates.iterator();
            while (updatesIterator.hasNext()) {
                Update update = updatesIterator.next();
                if(!(user.canRead("xnat:subjectData/project",update.getProject()))){
                    updatesIterator.remove();
                }
            }

            this.getResponse().setEntity(jsonRepresentation(updates));
        } catch (Throwable exception) {
            logger.error("Failed to get update.", exception);
        }
    }

    @Override public void handleDelete(){
        try {
            String project = SecureResource.getQueryVariable("project", getRequest());
            String date = SecureResource.getQueryVariable("date", getRequest());
            String update = SecureResource.getQueryVariable("update", getRequest());
            String link = SecureResource.getQueryVariable("link", getRequest());

            if((project != null) && (date != null) && (update != null) && (link != null)){
                XnatProjectdata proj = XnatProjectdata.getProjectByIDorAlias(project, user, false);
                if(!(user.canEdit(proj))){
                    throw new Exception("User does not have permission to edit this project.");
                }
                else{
                    List<Update> oldUpdates = XDAT.getContextService().getBean(UpdateService.class).getUpdates(project,date,update,link);
                    for(Update oldUpdate : oldUpdates){
                        XDAT.getContextService().getBean(UpdateService.class).delete(oldUpdate);
                    }
                }
            }
        } catch (Throwable exception) {
            logger.error("Failed to delete update.", exception);
        }
    }
    
    private Representation jsonRepresentation(final List<Update> updates) throws IOException {
        String json = requests.toJson(updates);
        Representation r = new StringRepresentation(json);
        r.setMediaType(MediaType.APPLICATION_JSON);
        return r;
    }


    @Override public void handlePut(){
        String project = SecureResource.getQueryVariable("project", getRequest());
        String date = SecureResource.getQueryVariable("date", getRequest());
        String update = SecureResource.getQueryVariable("update", getRequest());
        String link = SecureResource.getQueryVariable("link", getRequest());

        if((project != null) && (date != null) && (update != null) && (link != null)){

            try{
                XnatProjectdata proj = XnatProjectdata.getProjectByIDorAlias(project, user, false);
                if(!(user.canEdit(proj))){
                    throw new Exception("User does not have permission to edit this project.");
                }
                else{
                    Update newUpdate = new Update(project, date, update, link);
                    try{
                        XDAT.getContextService().getBean(UpdateService.class).create(newUpdate);
                    }catch(Throwable e){
                        logger.error("Exception creating update in database.",e);
                    }
                }
            }
            catch(Throwable e) {
                logger.error("Exception adding update.",e);
            }
        }
        else{
            logger.warn("Invalid project, date, update, or link.");
        }
    }
}
