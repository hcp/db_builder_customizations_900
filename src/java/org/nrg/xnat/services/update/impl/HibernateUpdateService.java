package org.nrg.xnat.services.update.impl;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.codehaus.jackson.map.DeserializationConfig;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.map.SerializationConfig;
import org.codehaus.jackson.map.annotate.JsonSerialize;
import org.nrg.framework.orm.hibernate.AbstractHibernateEntityService;
import org.nrg.xnat.daos.PublicationDAO;
import org.nrg.xnat.daos.UpdateDAO;
import org.nrg.xnat.entities.Publication;
import org.nrg.xnat.entities.Update;
import org.nrg.xnat.services.publication.PublicationService;
import org.nrg.xnat.services.update.UpdateService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.inject.Inject;
import javax.sql.DataSource;
import java.io.IOException;
import java.util.List;

/**
 * Created by mmckay01 on 1/29/14.
 */
@Service
public class HibernateUpdateService extends AbstractHibernateEntityService<Update, UpdateDAO> implements UpdateService {
    @Override
    @Transactional
    public List<Update> getUpdatesForProject(String project) {
        Update example = new Update();
        example.setProject(project);
        return _dao.findByExample(example, EXCLUSION_PROPERTIES);
    }

    @Override
    @Transactional
    public List<Update> getUpdates() {
        Update example = new Update();
        return _dao.findByExample(example, EXCLUSION_PROPERTIES_EXCLUDE_EVERYTHING);
    }

    @Override
    @Transactional
    public List<Update> getUpdates(String project, String date, String update, String link) {
        Update example = new Update();
        example.setProject(project);
        example.setDate(date);
        example.setUpdate(update);
        example.setLink(link);
        return _dao.findByExample(example, EXCLUSION_PROPERTIES_NOT_RELEVANT);
    }

    @Override
    public String toJson(final List<Update> updates) throws IOException{
        return _mapper.writeValueAsString(updates);
    }

    private static final String[] EXCLUSION_PROPERTIES = new String[] {"id", "enabled", "created", "timestamp", "disabled", "date", "update", "link"};
    private static final String[] EXCLUSION_PROPERTIES_NOT_RELEVANT = new String[] {"id", "enabled", "created", "timestamp", "disabled"};
    private static final String[] EXCLUSION_PROPERTIES_EXCLUDE_EVERYTHING = new String[] {"id", "enabled", "created", "timestamp", "disabled", "date", "update", "link", "project"};

    private static final Log _log = LogFactory.getLog(HibernateUpdateService.class);

    @Inject
    private UpdateDAO _dao;

    @Inject
    private DataSource _datasource;

    private final ObjectMapper _mapper = new ObjectMapper() {{
        getDeserializationConfig().set(DeserializationConfig.Feature.FAIL_ON_NULL_FOR_PRIMITIVES, false);
        getDeserializationConfig().set(DeserializationConfig.Feature.FAIL_ON_UNKNOWN_PROPERTIES, false);
        getDeserializationConfig().set(DeserializationConfig.Feature.ACCEPT_SINGLE_VALUE_AS_ARRAY, true);
        getDeserializationConfig().set(DeserializationConfig.Feature.WRAP_EXCEPTIONS, true);
        getSerializationConfig().setSerializationInclusion(JsonSerialize.Inclusion.NON_NULL);
        getSerializationConfig().set(SerializationConfig.Feature.FAIL_ON_EMPTY_BEANS, false);
    }};
}
