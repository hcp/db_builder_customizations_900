package org.nrg.xnat.services;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.ConcurrentModificationException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.inject.Inject;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nrg.framework.orm.hibernate.AbstractHibernateEntityService;
import org.nrg.xnat.daos.PackageFileDAO;
import org.nrg.xnat.entities.PackageFile;
import org.nrg.xnat.entities.PackageInfo;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class PackageFileService extends AbstractHibernateEntityService<PackageFile, PackageFileDAO> {
	
    private final List<PackageFile> pkgCache = Collections.synchronizedList(new ArrayList<PackageFile>());
	
    @Transactional
    public Map<String,PackageInfo> getPackageInfo(final Collection<String> projectPaths,final Collection<String> subjects) {
    	 final List queryList = _dao.getCountsAndSizes(projectPaths,subjects);
    	 Map<String,PackageInfo> infoMap = new HashMap<String,PackageInfo>();
    	 for (final Object o : queryList) {
    		 if (o.getClass().isAssignableFrom(Object[].class)) {
    			 Object[] oarray = (Object[])o;
    			 if ((oarray[0].getClass().isAssignableFrom(Long.class)) &&
    				 (oarray[1].getClass().isAssignableFrom(Long.class)) &&
    				 (oarray[2].getClass().isAssignableFrom(Long.class)) &&
    				 (oarray[3].getClass().isAssignableFrom(String.class))) {
    				 final PackageInfo info = new PackageInfo((Long)oarray[0],(Long)oarray[1],(Long)oarray[2]);
    				 infoMap.put(oarray[3].toString(), info);
    			 }
    		 }
    	 }
    	 return infoMap;
    }
    
    @Transactional
    public Long getCountByPackageIds(final Collection<String> projectPaths,final Collection<String> subjects,final Collection<String> packageIds) {
    	 final List queryList = _dao.getCountByPackageIds(projectPaths,subjects,packageIds);
    	 Map<String,PackageInfo> infoMap = new HashMap<String,PackageInfo>();
    	 for (final Object o : queryList) {
    		 if (o.getClass().isAssignableFrom(Long.class)) {
    			 return (Long)o;
    		 } else if (o.getClass().isAssignableFrom(Object[].class)) {
    			 Object[] oarray = (Object[])o;
    			 if ((oarray[0].getClass().isAssignableFrom(Long.class))) {
    				 return (Long)oarray[0];
    			 }
    		 }
    	 }
    	 return null;
    }
    
    @Transactional
    public PackageFile getPackageFileByPath(String path) {
    	if (pkgCache.isEmpty()) {
    		refreshCache();
    	} 
   		boolean continueV = false;
   		while (!continueV) {
   			try {
   				for (final PackageFile pkg : pkgCache) {
    				if (pkg.getFilePath().equals(path)) {
    					return pkg;
    				}
    			}
   				continueV = true;
    		} catch (ConcurrentModificationException e) {
	    		// Do nothing.  Try again until cache is finished refreshing.  Concurrency issues should only occur if 
    			// multiple users make calls quickly after Tomcat start while cache is being generated.
	   		}
    	}
    	return null;
    }
    
    @Transactional
    public synchronized void refreshCache() {
   		clearCache();
    	synchronized(pkgCache) {
    		pkgCache.addAll(_dao.findAllEnabled());
    	}
    }
    
    
    public void clearCache() {
    	synchronized(pkgCache) {
    		pkgCache.clear();
    	}
    }

    private static final Log _log = LogFactory.getLog(PackageFileService.class);

    @Inject
    private PackageFileDAO _dao;

    @Inject
    private javax.sql.DataSource _datasource;

    private static final String[] EXCLUSION_PROPERTIES = new String[] {"id", "enabled", "created", "timestamp", "disabled", "fileSize", "fileCount", "lastModified"};

}
