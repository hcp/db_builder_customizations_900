package org.nrg.xnat.services.amazonData;

import org.nrg.framework.orm.hibernate.BaseHibernateService;
import org.nrg.xnat.entities.AmazonData;

import java.io.IOException;
import java.util.List;

/**
 * Created by mmckay01 on 1/29/14.
 */
public interface AmazonDataService extends BaseHibernateService<AmazonData>{
    public static String SERVICE_NAME = "AmazonDataService";

    public List<AmazonData> getAmazonDataForUser(String userLogin);

    public String toJson(final List<AmazonData> amazonDataList) throws IOException;

    public String getKey();

    public void setKey(String key);

    public String getSecretKey();

    public void setSecretKey(String secretKey);

    public String getAwsGroup();

    public void setAwsGroup(String awsGroup);
	
    public String getAllUsers();

    public void setAllUsers(String allUsers);
}
