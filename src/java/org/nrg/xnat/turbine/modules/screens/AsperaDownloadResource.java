/**
 * Copyright (c) 2013 Washington University
 */
package org.nrg.xnat.turbine.modules.screens;

import java.net.MalformedURLException;
import java.net.URL;

import org.apache.turbine.util.RunData;
import org.apache.velocity.context.Context;
import org.nrg.xdat.om.ArcArchivespecification;
import org.nrg.xdat.turbine.modules.screens.SecureScreen;
import org.nrg.xnat.turbine.utils.ArcSpecManager;

public class AsperaDownloadResource extends SecureScreen {

    @Override
    protected void doBuildTemplate(final RunData data, final Context context)
            throws MalformedURLException {
        final ArcArchivespecification arc = ArcSpecManager.GetInstance();
        final URL url = new URL(arc.getSiteUrl());
        context.put("webapp", url);   // local host, to start
    }
}
