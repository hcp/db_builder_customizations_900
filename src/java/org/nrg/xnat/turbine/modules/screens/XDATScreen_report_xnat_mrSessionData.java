package org.nrg.xnat.turbine.modules.screens;

import org.nrg.xdat.turbine.utils.TurbineUtils;
import org.apache.turbine.util.RunData;
import org.apache.velocity.context.Context;
import org.nrg.xdat.turbine.modules.screens.SecureReport;
import org.apache.log4j.Logger;
import org.nrg.xdat.model.XnatImagescandataI;
import org.nrg.xdat.om.WrkWorkflowdata;
import org.nrg.xdat.om.XnatImagescandata;
import org.nrg.xdat.om.XnatMrsessiondata;
import org.nrg.xft.XFTItem;
import org.nrg.xft.search.CriteriaCollection;

import java.lang.Override;
import java.util.ArrayList;
import java.util.Iterator;
import org.apache.commons.lang.StringEscapeUtils;
import org.apache.commons.lang.StringUtils;
import org.nrg.xdat.XDAT;
import org.nrg.xdat.om.XnatProjectdata;
import org.nrg.xnat.services.datadictionary.DataDictionaryService;
import org.nrg.xdat.security.XDATUser;

/**
 * Created with IntelliJ IDEA.
 * User: mmckay01
 * Date: 7/30/13
 * Time: 3:13 PM
 * To change this template use File | Settings | File Templates.
 */
public class XDATScreen_report_xnat_mrSessionData extends HcpSecureReport {
    public static org.apache.log4j.Logger logger = org.apache.log4j.Logger.getLogger(XDATScreen_report_xnat_mrSessionData.class);

    @Override
    public void finalProcessing(RunData data, Context context) {
        // Handle tiers in the super class
        super.finalProcessing(data, context);

        try {
            XnatMrsessiondata mr = new XnatMrsessiondata(item);
            context.put("mr",mr);
            org.nrg.xft.search.CriteriaCollection cc = new CriteriaCollection("AND");
            cc.addClause("wrk:workflowData.ID",mr.getId());
            org.nrg.xft.collections.ItemCollection items = org.nrg.xft.search.ItemSearch.GetItems(cc,TurbineUtils.getUser(data),false);
            //Sort by Launch Time
            ArrayList workitems = items.getItems("wrk:workflowData.launch_time","DESC");
            Iterator iter = workitems.iterator();
            ArrayList workflows = new ArrayList();

            while (iter.hasNext())
            {
                WrkWorkflowdata vrc = new WrkWorkflowdata((XFTItem)iter.next());
                workflows.add(vrc);
            }

            context.put("workflows",workflows);

            // Handled in superclass
//            String project = "";
//
//            if(TurbineUtils.HasPassedParameter("project", data)){
//                project = (String)TurbineUtils.GetPassedParameter("project", data);
//            }
//            else{
//                project = mr.getProject();
//            }
//            context.put("project", project);

            for(XnatImagescandataI scan:mr.getSortedScans()) {
                ((XnatImagescandata) scan).setImageSessionData(mr);
            }
        } catch (Exception e) {
            logger.error("",e);
        }
    }
}
