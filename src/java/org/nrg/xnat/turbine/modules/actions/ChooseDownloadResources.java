//Copyright 2006 Harvard University / Washington University School of Medicine All Rights Reserved
/*
 * Created on Oct 17, 2006
 *
 */
package org.nrg.xnat.turbine.modules.actions;

import java.util.HashMap;
import java.util.Map;

import org.apache.commons.lang.StringEscapeUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.turbine.util.RunData;
import org.apache.velocity.context.Context;
import org.nrg.xdat.XDAT;
import org.nrg.xdat.turbine.modules.actions.SecureAction;
import org.nrg.xdat.turbine.utils.TurbineUtils;

public class ChooseDownloadResources extends SecureAction {

    private final Map<String, String> _parameters;

    public ChooseDownloadResources() {
	_parameters = new HashMap<String, String>();
    }

    @Override
    public void doPerform(final RunData data, final Context context) {
	initializeParameters(data);
	
	String project = getParameter("project");
	if (StringUtils.isBlank(project)) {
	    project = StringUtils.trimToEmpty(XDAT.getConfigService().getConfigContents("dashboard", "project"));
	}
	context.put("project", StringEscapeUtils.escapeJava(project));
	context.put("resource", StringEscapeUtils.escapeJava(getParameter("resource")));
	context.put("filePath", StringEscapeUtils.escapeJava(getParameter("filepath")));

	data.setScreenTemplate("AsperaDownloadResource.vm");
    }

    private void initializeParameters(final RunData data) {
	final Map<String, String> parameters = TurbineUtils.GetDataParameterHash(data);
	for (final String parameter : parameters.keySet()) {
	    _parameters.put(parameter,
		    StringEscapeUtils.unescapeXml(StringEscapeUtils.unescapeHtml(parameters.get(parameter))));
	}
    }

    private String getParameter(final String parameter) {
	return _parameters.get(parameter);
    }
}
